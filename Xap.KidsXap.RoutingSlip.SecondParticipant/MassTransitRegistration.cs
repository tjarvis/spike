﻿using System;
using Autofac;
using MassTransit;
using MassTransit.AutofacIntegration;
using MassTransit.Courier;
using Microsoft.Extensions.Configuration;
using Serilog;
using Xap.KidsXap.RoutingSlip.Common;

namespace Xap.KidsXap.RoutingSlip.SecondParticipant
{
    public class MassTransitRegistration : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterGeneric(typeof(AutofacExecuteActivityFactory<,>))
                .WithParameter(new NamedParameter("name", "message"))
                .As(typeof(ExecuteActivityFactory<,>))
                .InstancePerLifetimeScope();

            builder.RegisterGeneric(typeof(AutofacCompensateActivityFactory<,>))
                .WithParameter(new NamedParameter("name", "message"))
                .As(typeof(CompensateActivityFactory<,>))
                .InstancePerLifetimeScope();

            builder.RegisterAssemblyTypes(ThisAssembly)
                .AsClosedTypesOf(typeof(ExecuteActivity<>));

            builder.RegisterAssemblyTypes(ThisAssembly)
                .AsClosedTypesOf(typeof(CompensateActivity<>));
            
            builder.Register(context =>
            {
                var config = context.Resolve<IConfiguration>();
                var rabbitUrl = config["rabbitMq:Url"];
                var busControl = Bus.Factory.CreateUsingRabbitMq(cfg =>
                {
                    var host = cfg.Host(new Uri(rabbitUrl), rmq =>
                    {
                        rmq.Username("guest");
                        rmq.Password("guest");
                    });
                    var logger = context.Resolve<ILogger>();
                    cfg.UseSerilog(logger);

                    var compensateUri = new Uri($"{rabbitUrl}/{ReceiveEndpointsConstants.SecondParticipantCompensate}");
                    
                    cfg.ReceiveEndpoint(host, ReceiveEndpointsConstants.SecondParticipant, ep =>
                    {
                        ep.ExecuteActivityHost(compensateUri, context.Resolve<ExecuteActivityFactory<MutateTextActivity, IMutateArguments>>());
                    });

                    cfg.ReceiveEndpoint(host, ReceiveEndpointsConstants.SecondParticipantCompensate, ep =>
                    {
                        ep.CompensateActivityHost(context.Resolve<CompensateActivityFactory<MutateTextActivity, IMutateLog>>());
                    });

                });
                return busControl;
            }).As<IBusControl>().SingleInstance();
        }
    }
}
