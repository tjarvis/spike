﻿using System;
using System.Linq;
using System.Threading.Tasks;
using MassTransit.Courier;
using Serilog;
using Xap.KidsXap.RoutingSlip.Common;

namespace Xap.KidsXap.RoutingSlip.SecondParticipant
{
    public class MutateTextActivity : ExecuteActivity<IMutateArguments>, CompensateActivity<IMutateLog>
    {
        private readonly ILogger _logger;

        public MutateTextActivity(ILogger logger)
        {
            _logger = logger;
        }

        public async Task<ExecutionResult> Execute(ExecuteContext<IMutateArguments> context)
        {
            try
            {
                var txt = context.Arguments.Text;
                var log = new MutateLog {Text = txt};

                if (txt.ToLower() == "error2" || txt.ToLower() == "errorboth")
                {
                    throw new Exception("I don't know how to mutate that");
                }

                var mutatedText = new string(txt.Select(c => (char) (c + 1)).ToArray());

                _logger.Information($"The mutated text is {mutatedText} - doing something with it now.");
                await Task.CompletedTask;
                return context.Completed(log);
            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message);
                return context.Faulted(ex);
            }
                
        }

        public Task<CompensationResult> Compensate(CompensateContext<IMutateLog> context)
        {
            var log = context.Log;
            _logger.Information($"well damn; that mutation I did, I just undid it - the original string was {log.Text}");
            return Task.FromResult(context.Compensated());
        }
    }

}
