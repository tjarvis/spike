﻿using System;
using Autofac;
using MassTransit;
using Microsoft.Extensions.Configuration;
using Serilog;
using Xap.KidsXap.RoutingSlip.Common;

namespace Xap.KidsXap.RoutingSlip.SecondParticipant
{
    class Program
    {
        static void Main(string[] args)
        {
            // Configure App and setup dependency injection container.
            var builder = new ContainerBuilder();
            builder.RegisterModule<AppConfigurationRegistrations>();
            builder.RegisterModule<MassTransitRegistration>();
            var container = builder.Build();
            // Tell the user we are all set and ready to go.
            var config = container.Resolve<IConfiguration>();
            var logger = container.Resolve<ILogger>();
            logger.Information("Logging and configuration registered");
            logger.Information($"The rabbit url is : {config["rabbitMq:Url"]}");
            // Start the bus.
            var bus = container.Resolve<IBusControl>();
            bus.Start();
            try
            {
                logger.Information($"Application and message bus started - Listening for events on queue {ReceiveEndpointsConstants.SecondParticipant}");
                Console.ReadLine();
            }
            finally
            {
                bus.Stop();
                container.Dispose();
            }
        }
    }
}
