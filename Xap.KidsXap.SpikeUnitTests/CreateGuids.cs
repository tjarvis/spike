using System;
using System.Collections.Generic;
using Xunit;
using Xunit.Abstractions;

namespace Xap.KidsXap.SpikeUnitTests
{
    public class CreateGuids
    {

        private readonly ITestOutputHelper _out;

        public CreateGuids(ITestOutputHelper output)
        {
            _out = output;
        }
        
        [Fact]
        public void Test1()
        {

            var vars = new List<string>()
            {
                "Mandarin",
                "Spanish",
                "English",
                "Hindi",
                "Arabic",
                "Portuguese",
                "Bengali",
                "Russian",
                "Japanese",
                "Punjabi",
                "German",
                "Javanese",
                "Wu",
                "Malay",
                "Telugu",
                "Vietnamese",
                "Korean",
                "French",
                "Marathi",
                "Tamil",
                "Urdu",
                "Turkish",
                "Italian",
                "Yue",
                "Thai",
                "Gujarati",
                "Jin",
                "SouthernMin",
                "Persian",
                "Polish",
                "Pashto",
                "Kannada",
                "Xiang",
                "Malayalam",
                "Sundanese",
                "Hausa",
                "Odia",
                "Burmese",
                "Hakka",
                "Ukrainian",
                "Bhojpuri",
                "Tagalog",
                "Yoruba",
                "Maithili",
                "Uzbek",
                "Sindhi",
                "Amharic",
                "Fula",
                "Romanian",
                "Oromo",
                "Igbo",
                "Azerbaijani",
                "Awadhi",
                "GanChinese",
                "Cebuano",
                "Dutch",
                "Kurdish",
                "SerboCroatian",
                "Malagasy",
                "Saraiki",
                "Nepali",
                "Sinhalese",
                "Chittagonian",
                "Zhuang",
                "Khmer",
                "Turkmen",
                "Assamese",
                "Madurese",
                "Somali",
                "Marwari",
                "Magahi",
                "Haryanvi",
                "Hungarian",
                "Chhattisgarhi",
                "Greek",
                "Chewa",
                "Deccan",
                "Akan",
                "Kazakh",
                "NorthernMin",
                "Sylheti",
                "Zulu",
                "Czech",
                "Kinyarwanda",
                "Dhundhari",
                "HaitianCreole",
                "EasternMin",
                "Ilocano",
                "Quechua",
                "Kirundi",
                "Swedish",
                "Hmong",
                "Shona",
                "Uyghur",
                "HiligaynonIlonggo",
                "Mossi",
                "Xhosa",
                "Belarusian",
                "Balochi",
                "Konkani"
            };

            foreach (var v in vars)
            {
                var g = Guid.NewGuid();
                _out.WriteLine($"public static Guid {v} = new Guid(\"{g}\");");
            }
            
        }
    }
}