using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Xap.KidsXap.Validation;
using Xunit;
using Xunit.Abstractions;

namespace Xap.KidsXap.SpikeUnitTests
{
    public class TestValidations
    {
        private ITestOutputHelper _out;

        public TestValidations(ITestOutputHelper output)
        {
            _out = output;
        }

        [Fact]
        public void TestObjectValidatorWithAttributes()
        {
            var obj = new TestObject{TestStringProperty = "A value greater than 10 chars"};
            var results = obj.Validate().ToList();
            Assert.True(results.Any());
            _out.WriteLine(results.First().ErrorMessage);
        }

        [Fact]
        public void TestNameOfFunction()
        {
            var testObj = new TestObject();
            testObj.TestingTheNameOfFunction<TestValidations>(_out);
        }
        
    }

    public class TestObject
    {
        [MaxLength(10)]
        public string TestStringProperty { get; set; }

        public void TestingTheNameOfFunction<T>(ITestOutputHelper output)
        {
            output.WriteLine(typeof(T).Name);
        }
    }
    
}