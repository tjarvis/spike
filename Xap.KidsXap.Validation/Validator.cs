﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Reflection;

namespace Xap.KidsXap.Validation
{
    public class Validator<T>
    {
        private readonly ICollection<Func<T, ValidationResult>> _validations;

        public Validator()
        {
            _validations = new List<Func<T, ValidationResult>>();
        }

        public void AddValidation(Func<T, ValidationResult> validation)
        {
            if(validation == null) throw new ArgumentNullException();
            _validations.Add(validation);
        }

        public IEnumerable<ValidationResult> Validate(T data)
        {
            foreach (var v in _validations)
            {
                var result = v(data);  // ValidationResult.Success is null.
                if (result != null)
                {
                    yield return result;
                }
            }
        }

        /// <summary>
        /// This method uses reflection to add all static methods with the correct signature from the passed in type.
        /// </summary>
        /// <param name="type"></param>
        public void AddValidatorsFromType(Type type)
        {
            var methods = type.GetMethods(BindingFlags.Static | BindingFlags.Public);
            foreach (var mi in methods)
            {
                var func = Delegate.CreateDelegate(typeof(Func<T, ValidationResult>), mi, false);
                if (func != null)
                {
                    _validations.Add((Func<T, ValidationResult>)func);    
                }
            }
        }
        
        /// <summary>
        /// This method uses reflection to add all instance methods with the correct signature from the passed in class. 
        /// </summary>
        /// <param name="instance"></param>
        /// <typeparam name="TInstance"></typeparam>
        public void AddValidatorsFromInstance<TInstance>(TInstance instance) where TInstance : class
        {
            var methods = typeof(TInstance).GetMethods(BindingFlags.Instance | BindingFlags.Public);
            foreach (var mi in methods)
            {
                var func = Delegate.CreateDelegate(typeof(Func<T, ValidationResult>), instance, mi, false);
                if (func != null)
                {
                    _validations.Add((Func<T, ValidationResult>)func);    
                }
            }            
        }
    }

    public class Validator<T1, T2>
    {
        private readonly ICollection<Func<T1, T2, ValidationResult>> _validations;

        public Validator()
        {
            _validations = new List<Func<T1, T2, ValidationResult>>();
        }

        public void AddValidation(Func<T1, T2, ValidationResult> validation)
        {
            if(validation == null) throw new ArgumentNullException();
            _validations.Add(validation);
        }
        
        public IEnumerable<ValidationResult> Validate(T1 entity1, T2 entity2)
        {
            foreach (var v in _validations)
            {
                var result = v(entity1, entity2);  // ValidationResult.Success is null.
                if (result != null)
                {
                    yield return result;
                }
            }
        }
        
        /// <summary>
        /// This method uses reflection to add all static methods with the correct signature from the passed in type.
        /// </summary>
        /// <param name="type"></param>
        public void AddValidatorsFromType(Type type)
        {
            var methods = type.GetMethods(BindingFlags.Static | BindingFlags.Public);
            foreach (var mi in methods)
            {
                var func = Delegate.CreateDelegate(typeof(Func<T1, T2, ValidationResult>), mi, false);
                if (func != null)
                {
                    _validations.Add((Func<T1, T2, ValidationResult>)func);    
                }
            }
        }
        
        /// <summary>
        /// This method uses reflection to add all instance methods with the correct signature from the passed in class. 
        /// </summary>
        /// <param name="instance"></param>
        /// <typeparam name="TInstance"></typeparam>
        public void AddValidatorsFromInstance<TInstance>(TInstance instance) where TInstance : class
        {
            var methods = typeof(TInstance).GetMethods(BindingFlags.Instance | BindingFlags.Public);
            foreach (var mi in methods)
            {
                var func = Delegate.CreateDelegate(typeof(Func<T1, T2, ValidationResult>), instance, mi, false);
                if (func != null)
                {
                    _validations.Add((Func<T1, T2, ValidationResult>)func);    
                }
            }            
        }
    }
    
    public class Validator<T1, T2, T3>
    {
        private readonly ICollection<Func<T1, T2, T3, ValidationResult>> _validations;

        public Validator()
        {
            _validations = new List<Func<T1, T2, T3, ValidationResult>>();
        }

        public void AddValidation(Func<T1, T2, T3, ValidationResult> validation)
        {
            if(validation == null) throw new ArgumentNullException();
            _validations.Add(validation);
        }
        
        public IEnumerable<ValidationResult> Validate(T1 entity1, T2 entity2, T3 entity3)
        {
            foreach (var v in _validations)
            {
                var result = v(entity1, entity2, entity3);  // ValidationResult.Success is null.
                if (result != null)
                {
                    yield return result;
                }
            }
        }
        
        /// <summary>
        /// This method uses reflection to add all static methods with the correct signature from the passed in type.
        /// </summary>
        /// <param name="type"></param>
        public void AddValidatorsFromType(Type type)
        {
            var methods = type.GetMethods(BindingFlags.Static | BindingFlags.Public);
            foreach (var mi in methods)
            {
                var func = Delegate.CreateDelegate(typeof(Func<T1, T2, T3, ValidationResult>), mi, false);
                if (func != null)
                {
                    _validations.Add((Func<T1, T2, T3, ValidationResult>)func);    
                }
            }
        }
        
        /// <summary>
        /// This method uses reflection to add all instance methods with the correct signature from the passed in class. 
        /// </summary>
        /// <param name="instance"></param>
        /// <typeparam name="TInstance"></typeparam>
        public void AddValidatorsFromInstance<TInstance>(TInstance instance) where TInstance : class
        {
            var methods = typeof(TInstance).GetMethods(BindingFlags.Instance | BindingFlags.Public);
            foreach (var mi in methods)
            {
                var func = Delegate.CreateDelegate(typeof(Func<T1, T2, T3, ValidationResult>), instance, mi, false);
                if (func != null)
                {
                    _validations.Add((Func<T1, T2, T3, ValidationResult>)func);    
                }
            }            
        }
    }
    
}