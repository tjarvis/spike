﻿namespace Xap.KidsXap.ClaimsDemonstration
{
    partial class FrmClaimsDemonstration
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tcMain = new System.Windows.Forms.TabControl();
            this.tpMapped = new System.Windows.Forms.TabPage();
            this.tvTenants = new System.Windows.Forms.TreeView();
            this.label1 = new System.Windows.Forms.Label();
            this.btnRefresh = new System.Windows.Forms.Button();
            this.cbIssueClaimsToUser = new System.Windows.Forms.ComboBox();
            this.btnCreate = new System.Windows.Forms.Button();
            this.btnAssign = new System.Windows.Forms.Button();
            this.cbRoles = new System.Windows.Forms.ComboBox();
            this.label18 = new System.Windows.Forms.Label();
            this.cbAssignToUser = new System.Windows.Forms.ComboBox();
            this.label17 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.cbIssuer = new System.Windows.Forms.ComboBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.lvRoleClaims = new System.Windows.Forms.ListView();
            this.chClaim = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.chTennantId = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.btnCreateRoleClaims = new System.Windows.Forms.Button();
            this.lstRolePermissions = new System.Windows.Forms.CheckedListBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtRoleName = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.tvUserOrganisation = new System.Windows.Forms.TreeView();
            this.label5 = new System.Windows.Forms.Label();
            this.cbUsers = new System.Windows.Forms.ComboBox();
            this.cbCheckAllPermissions = new System.Windows.Forms.CheckBox();
            this.tcMain.SuspendLayout();
            this.tpMapped.SuspendLayout();
            this.SuspendLayout();
            // 
            // tcMain
            // 
            this.tcMain.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tcMain.Controls.Add(this.tpMapped);
            this.tcMain.Location = new System.Drawing.Point(12, 12);
            this.tcMain.Name = "tcMain";
            this.tcMain.SelectedIndex = 0;
            this.tcMain.Size = new System.Drawing.Size(1469, 815);
            this.tcMain.TabIndex = 0;
            // 
            // tpMapped
            // 
            this.tpMapped.Controls.Add(this.cbCheckAllPermissions);
            this.tpMapped.Controls.Add(this.tvTenants);
            this.tpMapped.Controls.Add(this.label1);
            this.tpMapped.Controls.Add(this.btnRefresh);
            this.tpMapped.Controls.Add(this.cbIssueClaimsToUser);
            this.tpMapped.Controls.Add(this.btnCreate);
            this.tpMapped.Controls.Add(this.btnAssign);
            this.tpMapped.Controls.Add(this.cbRoles);
            this.tpMapped.Controls.Add(this.label18);
            this.tpMapped.Controls.Add(this.cbAssignToUser);
            this.tpMapped.Controls.Add(this.label17);
            this.tpMapped.Controls.Add(this.label16);
            this.tpMapped.Controls.Add(this.cbIssuer);
            this.tpMapped.Controls.Add(this.label15);
            this.tpMapped.Controls.Add(this.label14);
            this.tpMapped.Controls.Add(this.label13);
            this.tpMapped.Controls.Add(this.label12);
            this.tpMapped.Controls.Add(this.label11);
            this.tpMapped.Controls.Add(this.label10);
            this.tpMapped.Controls.Add(this.label9);
            this.tpMapped.Controls.Add(this.label8);
            this.tpMapped.Controls.Add(this.lvRoleClaims);
            this.tpMapped.Controls.Add(this.btnCreateRoleClaims);
            this.tpMapped.Controls.Add(this.lstRolePermissions);
            this.tpMapped.Controls.Add(this.label7);
            this.tpMapped.Controls.Add(this.txtRoleName);
            this.tpMapped.Controls.Add(this.label6);
            this.tpMapped.Controls.Add(this.tvUserOrganisation);
            this.tpMapped.Controls.Add(this.label5);
            this.tpMapped.Controls.Add(this.cbUsers);
            this.tpMapped.Location = new System.Drawing.Point(4, 22);
            this.tpMapped.Name = "tpMapped";
            this.tpMapped.Padding = new System.Windows.Forms.Padding(3);
            this.tpMapped.Size = new System.Drawing.Size(1461, 789);
            this.tpMapped.TabIndex = 1;
            this.tpMapped.Text = "Claims Generation";
            this.tpMapped.UseVisualStyleBackColor = true;
            // 
            // tvTenants
            // 
            this.tvTenants.CheckBoxes = true;
            this.tvTenants.Location = new System.Drawing.Point(20, 240);
            this.tvTenants.Name = "tvTenants";
            this.tvTenants.Size = new System.Drawing.Size(356, 111);
            this.tvTenants.TabIndex = 31;
            this.tvTenants.BeforeCheck += new System.Windows.Forms.TreeViewCancelEventHandler(this.tvUserOrganisation_BeforeCheck);
            this.tvTenants.AfterCheck += new System.Windows.Forms.TreeViewEventHandler(this.tvUserOrganisation_AfterCheck);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(17, 224);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 13);
            this.label1.TabIndex = 30;
            this.label1.Text = "Tenant";
            // 
            // btnRefresh
            // 
            this.btnRefresh.Location = new System.Drawing.Point(25, 7);
            this.btnRefresh.Name = "btnRefresh";
            this.btnRefresh.Size = new System.Drawing.Size(108, 23);
            this.btnRefresh.TabIndex = 29;
            this.btnRefresh.Text = "Refresh User Lists";
            this.btnRefresh.UseVisualStyleBackColor = true;
            this.btnRefresh.Click += new System.EventHandler(this.btnRefresh_Click);
            // 
            // cbIssueClaimsToUser
            // 
            this.cbIssueClaimsToUser.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbIssueClaimsToUser.FormattingEnabled = true;
            this.cbIssueClaimsToUser.Location = new System.Drawing.Point(994, 9);
            this.cbIssueClaimsToUser.Name = "cbIssueClaimsToUser";
            this.cbIssueClaimsToUser.Size = new System.Drawing.Size(377, 21);
            this.cbIssueClaimsToUser.TabIndex = 28;
            // 
            // btnCreate
            // 
            this.btnCreate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnCreate.Location = new System.Drawing.Point(20, 759);
            this.btnCreate.Name = "btnCreate";
            this.btnCreate.Size = new System.Drawing.Size(75, 23);
            this.btnCreate.TabIndex = 27;
            this.btnCreate.Text = "Create";
            this.btnCreate.UseVisualStyleBackColor = true;
            this.btnCreate.Click += new System.EventHandler(this.btnCreate_Click);
            // 
            // btnAssign
            // 
            this.btnAssign.Location = new System.Drawing.Point(447, 559);
            this.btnAssign.Name = "btnAssign";
            this.btnAssign.Size = new System.Drawing.Size(75, 23);
            this.btnAssign.TabIndex = 26;
            this.btnAssign.Text = "Assign";
            this.btnAssign.UseVisualStyleBackColor = true;
            this.btnAssign.Click += new System.EventHandler(this.btnAssign_Click);
            // 
            // cbRoles
            // 
            this.cbRoles.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbRoles.FormattingEnabled = true;
            this.cbRoles.Location = new System.Drawing.Point(444, 204);
            this.cbRoles.Name = "cbRoles";
            this.cbRoles.Size = new System.Drawing.Size(289, 21);
            this.cbRoles.TabIndex = 25;
            this.cbRoles.SelectedIndexChanged += new System.EventHandler(this.cbRoles_SelectedIndexChanged);
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(444, 188);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(29, 13);
            this.label18.TabIndex = 24;
            this.label18.Text = "Role";
            // 
            // cbAssignToUser
            // 
            this.cbAssignToUser.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbAssignToUser.FormattingEnabled = true;
            this.cbAssignToUser.Location = new System.Drawing.Point(447, 513);
            this.cbAssignToUser.Name = "cbAssignToUser";
            this.cbAssignToUser.Size = new System.Drawing.Size(292, 21);
            this.cbAssignToUser.TabIndex = 23;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(444, 485);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(79, 13);
            this.label17.TabIndex = 22;
            this.label17.Text = "Assign To User";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(444, 246);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(92, 13);
            this.label16.TabIndex = 21;
            this.label16.Text = "Available Tenants";
            // 
            // cbIssuer
            // 
            this.cbIssuer.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbIssuer.FormattingEnabled = true;
            this.cbIssuer.Location = new System.Drawing.Point(444, 149);
            this.cbIssuer.Name = "cbIssuer";
            this.cbIssuer.Size = new System.Drawing.Size(292, 21);
            this.cbIssuer.TabIndex = 20;
            this.cbIssuer.SelectedIndexChanged += new System.EventHandler(this.cbIssuer_SelectedIndexChanged);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(441, 131);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(109, 13);
            this.label15.TabIndex = 19;
            this.label15.Text = "User (Issuing the role)";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(444, 102);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(272, 13);
            this.label14.TabIndex = 18;
            this.label14.Text = "3. Claims are only issued if the tenant subscription allows";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(444, 75);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(316, 13);
            this.label13.TabIndex = 17;
            this.label13.Text = "2. The assigning user must have the claim permission/mange/role";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(444, 49);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(367, 13);
            this.label12.TabIndex = 16;
            this.label12.Text = "1. The available organisation heirachy is driven by the assigning users claims";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(443, 16);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(121, 20);
            this.label11.TabIndex = 15;
            this.label11.Text = "Assigning a role";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(22, 101);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(380, 13);
            this.label10.TabIndex = 14;
            this.label10.Text = "2. The available permissions are limited to the permissions of the assigning user" +
    ".";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(22, 75);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(354, 13);
            this.label9.TabIndex = 13;
            this.label9.Text = "1. The user creating the role must have the claim permission/manage/role";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(18, 42);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(119, 20);
            this.label8.TabIndex = 12;
            this.label8.Text = "Creating a Role";
            // 
            // lvRoleClaims
            // 
            this.lvRoleClaims.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lvRoleClaims.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.chClaim,
            this.chTennantId});
            this.lvRoleClaims.Location = new System.Drawing.Point(857, 45);
            this.lvRoleClaims.Name = "lvRoleClaims";
            this.lvRoleClaims.Size = new System.Drawing.Size(578, 667);
            this.lvRoleClaims.TabIndex = 11;
            this.lvRoleClaims.UseCompatibleStateImageBehavior = false;
            this.lvRoleClaims.View = System.Windows.Forms.View.Details;
            // 
            // chClaim
            // 
            this.chClaim.Text = "Claim";
            this.chClaim.Width = 287;
            // 
            // chTennantId
            // 
            this.chTennantId.Text = "Tenant ID";
            this.chTennantId.Width = 196;
            // 
            // btnCreateRoleClaims
            // 
            this.btnCreateRoleClaims.Location = new System.Drawing.Point(857, 9);
            this.btnCreateRoleClaims.Name = "btnCreateRoleClaims";
            this.btnCreateRoleClaims.Size = new System.Drawing.Size(131, 23);
            this.btnCreateRoleClaims.TabIndex = 10;
            this.btnCreateRoleClaims.Text = "Issue Claims to User";
            this.btnCreateRoleClaims.UseVisualStyleBackColor = true;
            this.btnCreateRoleClaims.Click += new System.EventHandler(this.btnCreateRoleClaims_Click);
            // 
            // lstRolePermissions
            // 
            this.lstRolePermissions.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.lstRolePermissions.CheckOnClick = true;
            this.lstRolePermissions.FormattingEnabled = true;
            this.lstRolePermissions.Location = new System.Drawing.Point(20, 379);
            this.lstRolePermissions.Name = "lstRolePermissions";
            this.lstRolePermissions.Size = new System.Drawing.Size(356, 349);
            this.lstRolePermissions.TabIndex = 9;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(17, 363);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(108, 13);
            this.label7.TabIndex = 8;
            this.label7.Text = "Available Permissions";
            // 
            // txtRoleName
            // 
            this.txtRoleName.Location = new System.Drawing.Point(20, 191);
            this.txtRoleName.Name = "txtRoleName";
            this.txtRoleName.Size = new System.Drawing.Size(356, 20);
            this.txtRoleName.TabIndex = 6;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(19, 175);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(60, 13);
            this.label6.TabIndex = 5;
            this.label6.Text = "Role Name";
            // 
            // tvUserOrganisation
            // 
            this.tvUserOrganisation.CheckBoxes = true;
            this.tvUserOrganisation.Location = new System.Drawing.Point(444, 265);
            this.tvUserOrganisation.Name = "tvUserOrganisation";
            this.tvUserOrganisation.Size = new System.Drawing.Size(298, 198);
            this.tvUserOrganisation.TabIndex = 4;
            this.tvUserOrganisation.BeforeCheck += new System.Windows.Forms.TreeViewCancelEventHandler(this.tvUserOrganisation_BeforeCheck);
            this.tvUserOrganisation.AfterCheck += new System.Windows.Forms.TreeViewEventHandler(this.tvUserOrganisation_AfterCheck);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(19, 128);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(114, 13);
            this.label5.TabIndex = 3;
            this.label5.Text = "User (creating the role)";
            // 
            // cbUsers
            // 
            this.cbUsers.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbUsers.FormattingEnabled = true;
            this.cbUsers.Location = new System.Drawing.Point(20, 144);
            this.cbUsers.Name = "cbUsers";
            this.cbUsers.Size = new System.Drawing.Size(270, 21);
            this.cbUsers.TabIndex = 2;
            this.cbUsers.SelectedIndexChanged += new System.EventHandler(this.cbUsers_SelectedIndexChanged);
            // 
            // cbCheckAllPermissions
            // 
            this.cbCheckAllPermissions.AutoSize = true;
            this.cbCheckAllPermissions.Location = new System.Drawing.Point(305, 362);
            this.cbCheckAllPermissions.Name = "cbCheckAllPermissions";
            this.cbCheckAllPermissions.Size = new System.Drawing.Size(71, 17);
            this.cbCheckAllPermissions.TabIndex = 32;
            this.cbCheckAllPermissions.Text = "Check All";
            this.cbCheckAllPermissions.UseVisualStyleBackColor = true;
            this.cbCheckAllPermissions.CheckedChanged += new System.EventHandler(this.cbCheckAllPermissions_CheckedChanged);
            // 
            // FrmClaimsDemonstration
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1493, 839);
            this.Controls.Add(this.tcMain);
            this.Name = "FrmClaimsDemonstration";
            this.Text = "Claims Demonstration";
            this.Load += new System.EventHandler(this.FrmClaimsDemonstration_Load);
            this.tcMain.ResumeLayout(false);
            this.tpMapped.ResumeLayout(false);
            this.tpMapped.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tcMain;
        private System.Windows.Forms.TabPage tpMapped;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox cbUsers;
        private System.Windows.Forms.TreeView tvUserOrganisation;
        private System.Windows.Forms.TextBox txtRoleName;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.CheckedListBox lstRolePermissions;
        private System.Windows.Forms.ListView lvRoleClaims;
        private System.Windows.Forms.ColumnHeader chClaim;
        private System.Windows.Forms.ColumnHeader chTennantId;
        private System.Windows.Forms.Button btnCreateRoleClaims;
        private System.Windows.Forms.Button btnCreate;
        private System.Windows.Forms.Button btnAssign;
        private System.Windows.Forms.ComboBox cbRoles;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.ComboBox cbAssignToUser;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.ComboBox cbIssuer;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox cbIssueClaimsToUser;
        private System.Windows.Forms.Button btnRefresh;
        private System.Windows.Forms.TreeView tvTenants;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.CheckBox cbCheckAllPermissions;
    }
}

