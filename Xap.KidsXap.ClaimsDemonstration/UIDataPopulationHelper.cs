﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using Xap.KidsXap.ClaimsDemonstration.Data;
using Xap.KidsXap.ClaimsDemonstration.Domain;

namespace Xap.KidsXap.ClaimsDemonstration
{
    public static class UiDataPopulationHelper
    {
        public static void AddOrganisationToTree(this TreeView tree, Organisation org)
        {
            tree.BeginUpdate();
            try
            {
                var node = new TreeNode(org.ToString()){Tag = org.Id};
                foreach (var p in org.Providers)
                {
                    var provNode = new TreeNode(p.ToString()){Tag = p.Id};
                    foreach (var c in p.Centres)
                    {
                        var centreNode = new TreeNode(c.ToString()){Tag = c.Id};
                        provNode.Nodes.Add(centreNode);
                    }
                    node.Nodes.Add(provNode);
                }
                tree.Nodes.Add(node);
            }
            finally
            {
                tree.EndUpdate();
            }
        }

        public static void AddAllOrganisationsToTree(this TreeView tree)
        {
            tree.Nodes.Clear();
            foreach (var org in Repository.Organisations)
            {
                tree.AddOrganisationToTree(org);
            }
        }

        public static void AddAllUsers(this ComboBox combo)
        {
            combo.BeginUpdate();
            try
            {
                combo.Items.Clear();
                foreach (var u in Repository.Users.Where(x => x.IsSystemAdmin == false))
                {
                    
                    combo.Items.Add(u);
                }
            }
            finally
            {
                combo.EndUpdate();
            }
        }

        public static void AddUsersWithManageRoleClaim(this ComboBox combo)
        {
            combo.BeginUpdate();
            try
            {
                combo.Items.Clear();
                // First Add any SystemAdministrators...
                var admins = Repository.Users.Where(u => u.IsSystemAdmin);
                foreach (var admin in admins)
                {
                    combo.Items.Add(admin);
                }
                // Add users with manage roles claim
                var managers = Repository.GetUsersWithManageRoleClaim();
                if (managers != null)
                {
                    foreach (var manager in managers)
                    {
                        combo.Items.Add(manager);
                    }
                }
            }
            finally
            {
                combo.EndUpdate();
            }
        }

        public static void AddRolesForUser(this ComboBox combo, UserProfile user)
        {
            combo.BeginUpdate();
            try
            {
                combo.Items.Clear();
                var roles = Repository.GetRolesAvailableToUserToAssignOrUpdate(user);
                foreach (var r in roles)
                {
                    combo.Items.Add(r);
                }
            }
            finally
            {
                combo.EndUpdate();
            }
        }

        public static void AddHierarchyFromFlatList(this TreeView tree, IEnumerable<Guid> tenants)
        {
            tree.BeginUpdate();
            var list = tenants.ToDictionary(key => key, value => value);
            try
            {
                // Add them all 
                tree.AddAllOrganisationsToTree();
                // Set them to readonly
                foreach (var n in tree.Nodes.Descendants())
                {
                    var id = (Guid) n.Tag;
                    if (!list.ContainsKey(id))
                    {
                        n.ForeColor = Color.BurlyWood; // :-) never even heard of this color before 
                    }
                }

                var remove = new List<TreeNode>();
                foreach (var node in tree.Nodes.Cast<TreeNode>())
                {
                    if (node != null)
                    {
                        if (node.Nodes.Descendants().All(n => n.ForeColor == Color.BurlyWood))
                        {
                            //node.Remove();
                            remove.Add(node);
                        }
                    }
                }

                foreach (var node in tree.Nodes.Cast<TreeNode>())
                {
                    if (node != null)
                    {
                        foreach (var prov in node.Nodes.Cast<TreeNode>())
                        {
                            if (prov != null)
                            {
                                if (prov.Nodes.Descendants().All(p => p.ForeColor == Color.BurlyWood))
                                {
                                    //prov.Remove();
                                    remove.Add(prov);
                                }
                            }
                        }
                    }
                }

                foreach (var node in remove)
                {
                     node.Remove();  
                }
            }
            finally
            {
                tree.EndUpdate();
            }
        }


        public static void PopulateList(this ListBox list, IEnumerable<string> values, bool appendValues = false)
        {
            list.BeginUpdate();
            try
            {
                if (!appendValues) list.Items.Clear();
                list.Items.AddRange(values.Cast<object>().ToArray());
            }
            finally
            {
                list.EndUpdate();
            }
        }

        public static void PopulateList(this CheckedListBox list, IEnumerable<string> values, bool appendValues = false)
        {
            list.BeginUpdate();
            try
            {
                if(!appendValues) list.Items.Clear();
                list.Items.AddRange(values.Cast<object>().ToArray());
            }
            finally
            {
                list.EndUpdate();
            }
        }

        public static void PopulateListViewWithXapClaims(this ListView listview, IEnumerable<XapClaim> claims)
        {
            listview.BeginUpdate();
            try
            {
                foreach (var x in claims)
                {
                    var item = new ListViewItem(x.Claim);

                    if (x.TenantId.HasValue)
                    {
                        var t = Repository.GetTenantById(x.TenantId.Value);
                        item.SubItems.Add(t.ToString());
                    }
                    listview.Items.Add(item);
                }
            }
            finally
            {
                listview.EndUpdate();
            }
        }

        internal static IEnumerable<TreeNode> Descendants(this TreeNodeCollection c)
        {
            foreach (var node in c.OfType<TreeNode>())
            {
                yield return node;

                foreach (var child in node.Nodes.Descendants())
                {
                    yield return child;
                }
            }
        }

        internal static void CheckAll(this CheckedListBox list, bool value)
        {
            list.BeginUpdate();
            try
            {
                var count = list.Items.Count;

                for (int i = 0; i < count; i++)
                {
                    list.SetItemChecked(i, value);
                }
            }
            finally
            {
                list.EndUpdate();
            }
        }

        internal static void CheckAllChildNodes(this TreeNode treeNode, bool nodeChecked)
        {
            foreach (TreeNode node in treeNode.Nodes)
            {
                node.Checked = nodeChecked;
                if (node.Nodes.Count > 0)
                {
                    node.CheckAllChildNodes(nodeChecked);
                }
            }
        }
    }
}
