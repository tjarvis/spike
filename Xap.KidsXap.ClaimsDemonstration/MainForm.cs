﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Eventing.Reader;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using Xap.KidsXap.ClaimsDemonstration.Data;
using Xap.KidsXap.ClaimsDemonstration.Domain;

namespace Xap.KidsXap.ClaimsDemonstration
{
    public partial class FrmClaimsDemonstration : Form
    {

        public FrmClaimsDemonstration()
        {
            InitializeComponent();
        }

        private void FrmClaimsDemonstration_Load(object sender, EventArgs e)
        {
            Repository.Load();
            RefreshUserLists();
        }

        private void cbUsers_SelectedIndexChanged(object sender, EventArgs e)
        {
            var user = (UserProfile) cbUsers.Items[cbUsers.SelectedIndex];
            var issuer = new ClaimsIssuer();
            var permissions = issuer.ClaimsForUser(user).Where(c => c.Claim.StartsWith("permission"));
            lstRolePermissions.PopulateList(permissions.Select(x => x.Claim).Distinct());
            if (user.IsSystemAdmin)
            {
                tvTenants.AddAllOrganisationsToTree();
            }
            else
            {
                var ids = permissions.Where(p => p.TenantId.HasValue).Select(x => x.TenantId.Value).Distinct();
                tvTenants.AddHierarchyFromFlatList(ids);
            }
        }

        private void btnCreateRoleClaims_Click(object sender, EventArgs e)
        {
            if( cbIssueClaimsToUser.SelectedItem is UserProfile user)
            { 
                var claims = new ClaimsIssuer().ClaimsForUser(user);
                lvRoleClaims.Items.Clear();
                lvRoleClaims.PopulateListViewWithXapClaims(claims);
            }
            else
            {
                MessageBox.Show("You must first select a user", "Select user", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void tvUserOrganisation_AfterCheck(object sender, TreeViewEventArgs e)
        {
            if (e.Action != TreeViewAction.Unknown)
            {
                if (e.Node.Nodes.Count > 0)
                {
                    e.Node.CheckAllChildNodes(e.Node.Checked);
                }
            }
        }

        private void tvUserOrganisation_BeforeCheck(object sender, TreeViewCancelEventArgs e)
        {
            if (e.Action != TreeViewAction.Unknown)
            {
                if ((e.Node.Parent != null && e.Node.Parent.Checked) || e.Node.ForeColor == Color.BurlyWood)
                {
                    e.Cancel = true;
                }
            }
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            RefreshUserLists();
        }

        private void RefreshUserLists()
        {
            // Create Role
            cbUsers.AddUsersWithManageRoleClaim();
            // Assign Role
            cbIssuer.AddUsersWithManageRoleClaim();
            // Assign Role To
            cbAssignToUser.AddAllUsers();
            // Issue Claims To
            cbIssueClaimsToUser.AddAllUsers();
        }

        private void btnCreate_Click(object sender, EventArgs e)
        {
            var role = txtRoleName.Text;
            if (!string.IsNullOrEmpty(role))
            {
                var permissions = lstRolePermissions.CheckedItems.Cast<string>();
                var tenants = tvTenants.Nodes.Descendants().Where(n => n.Checked).Select(n => (Guid)n.Tag);
                Repository.CreateRole(role, permissions, tenants);
                txtRoleName.Text = string.Empty;
                tvTenants.BeforeCheck -= tvUserOrganisation_BeforeCheck;
                tvTenants.AfterCheck -= tvUserOrganisation_AfterCheck;
                foreach (var n in tvTenants.Nodes.Descendants())
                {
                    n.Checked = false;
                }
                tvTenants.BeforeCheck += tvUserOrganisation_BeforeCheck;
                tvTenants.AfterCheck += tvUserOrganisation_AfterCheck;
                lstRolePermissions.CheckAll(false);
            }
            else
            {
                MessageBox.Show("You must enter a role name", "Enter Role Name", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void cbCheckAllPermissions_CheckedChanged(object sender, EventArgs e)
        {
            lstRolePermissions.CheckAll(cbCheckAllPermissions.Checked);
        }

        private void cbIssuer_SelectedIndexChanged(object sender, EventArgs e)
        {
            var combo = (ComboBox) sender;
            cbRoles.AddRolesForUser((UserProfile)combo.SelectedItem);
            
        }

        private void cbRoles_SelectedIndexChanged(object sender, EventArgs e)
        {
            // Get Tenants for User
            var user = (UserProfile) cbIssuer.SelectedItem;
            IEnumerable<Guid> userTenants;
            if (user.IsSystemAdmin)
            {
                // All of them.
                userTenants = Repository.GetAllTenantsFromOrgHierarchy(Repository.Organisations);
            }
            else
            {
                userTenants = new ClaimsIssuer().ClaimsForUser(user).Where(x => x.TenantId.HasValue).Select(c => c.TenantId.Value).Distinct();
            }

            // The Intersection of the Role Tenants.
            var role = (Role) cbRoles.SelectedItem;
            var tenants = userTenants.Intersect(role.Tenants);

            // Populate the tree view with the new list.
            tvUserOrganisation.AddHierarchyFromFlatList(tenants);

        }

        private void btnAssign_Click(object sender, EventArgs e)
        {
            if (cbRoles.SelectedItem is Role role)
            {

                if (cbAssignToUser.SelectedItem is UserProfile user)
                {
                    var ids = tvUserOrganisation.Nodes.Descendants().Where(x => x.Checked).Select(n => (Guid) n.Tag);
                    Repository.AssignRole(user, role, ids);
                    RefreshUserLists();
                }
                else
                {
                    MessageBox.Show("You need to select a user to assign to.", "Assign User", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
            }
            else
            {
                MessageBox.Show("You need to select a role.", "Select Role", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }
    }
}
