﻿using System;
using System.Collections.Generic;
using System.Linq;
using Xap.KidsXap.ClaimsDemonstration.Domain;

namespace Xap.KidsXap.ClaimsDemonstration.Data
{
    public static class Repository
    {

        public static List<Role> Roles { get; set; }
        public static List<UserProfile> Users { get; set; }
        public static List<Organisation> Organisations { get; set; }

        static Repository()
        {
            Roles = new List<Role>();
            Users = new List<UserProfile>();
            Organisations = new List<Organisation>();
        }

        public static void Load()
        {
            Users.AddRange(new FakeUserProfile().Generate());
            Organisations.Add(new FakeOrganisation().WithSubscriptionType(Subscription.Basic).Generate());
            Organisations.Add(new FakeOrganisation().WithSubscriptionType(Subscription.Standard).Generate());
            Organisations.Add(new FakeOrganisation().WithSubscriptionType(Subscription.Premium).Generate());
        }

        public static void CreateRole(string roleName, IEnumerable<string> permissions, IEnumerable<Guid> tenants)
        {
            var role = new Role
            {
                Id = Guid.NewGuid(),
                Name = roleName,
            };
            foreach (var p in permissions)
            {
                role.Permissions.Add(p);
            }

            foreach (var t in tenants)
            {
                role.Tenants.Add(t);
            }

            Roles.Add(role);
        }

        public static IEnumerable<Guid> GetAllTenantsFromOrgHierarchy(IEnumerable<Organisation> orgs)
        {
            foreach (var o in orgs)
            {
                yield return o.Id;
                if (o.Providers.Any())
                {
                    foreach (var p in o.Providers)
                    {
                        yield return p.Id;
                        if (p.Centres.Any())
                        {
                            foreach (var c in p.Centres)
                            {
                                yield return c.Id;
                            }
                        }
                    }
                }
            }
        }

        public static IEnumerable<UserProfile> GetUsersWithManageRoleClaim()
        {
            return (from u in Users
                from assignedRole in u.Roles
                join r in Roles on assignedRole.RoleId equals r.Id
                from x in r.Permissions
                where x.Contains(Permission.Roles.ToManageClaim())
                select u).Distinct();
        }

        public static IEnumerable<Role> GetRolesAvailableToUserToAssignOrUpdate(UserProfile user)
        {
            if (user.IsSystemAdmin)
            {
                // Return them all..
                foreach (var r in Roles)
                {
                    yield return r;
                }
            }
            else
            {
                var issuer = new ClaimsIssuer();
                var manageRoleTenantIds = issuer.ClaimsForUser(user).Where(x => x.Claim.Equals("permission/manage/roles")).Select(x => x.TenantId ?? Guid.Empty).ToList();
                foreach (var r in Roles)
                {
                    if (r.Tenants.Intersect(manageRoleTenantIds).Any())
                    {
                        yield return r;
                    }
                }
            }
        }


        public static void AssignRole(UserProfile user, Role role, IEnumerable<Guid> tenants)
        {
            var assignment = new RoleAssignment {RoleId = role.Id};
            assignment.TenantIds.AddRange(tenants);
            user.Roles.Add(assignment);
        }

        public static IEnumerable<TenantSubscription> GetTenantSubscriptions()
        {
            foreach (var org in Organisations)
            {
                yield return new TenantSubscription {Id = org.Id, Subscription = org.Subscription};
                foreach (var provider in org.Providers)
                {
                    if (!provider.Subscription.HasValue)
                    {
                        provider.Subscription = org.Subscription;
                    }
                    yield return new TenantSubscription { Id = provider.Id, Subscription = provider.Subscription};
                    foreach (var centre in provider.Centres)
                    {
                        if (!centre.Subscription.HasValue)
                        {
                            centre.Subscription = provider.Subscription;
                        }
                        yield return new TenantSubscription {Id = centre.Id, Subscription = centre.Subscription};
                    }
                }
            }
        }

        public static object GetTenantById(Guid id)
        {
            foreach (var org in Organisations)
            {
                if (org.Id == id)
                {
                    return org;
                }

                foreach (var p in org.Providers)
                {
                    if (p.Id == id)
                    {
                        return p;
                    }

                    foreach (var c in p.Centres)
                    {
                        if (c.Id == id)
                        {
                            return c;
                        }
                    }
                }
            }
            throw new Exception("Not Found.");
        }
    }

    public class TenantSubscription
    {
        public Guid Id { get; set; }
        public Subscription? Subscription { get; set; }
    }

}
