﻿using System;
using System.Collections.Generic;
using System.Linq;
using Bogus;
using Xap.KidsXap.ClaimsDemonstration.Domain;

namespace Xap.KidsXap.ClaimsDemonstration.Data
{
    public class FakeUserProfile
    {
        public IEnumerable<UserProfile> Generate()
        {
            // System Admin.
            yield return new UserProfile
            {
                Id = Guid.NewGuid(),
                FirstName = "Tim",
                LastName = "Jarvis",
                EmailAddress = "tim.jarvis@playtimesolutions.com.au",
                IsSystemAdmin = true
            };

            var f = new Faker();

            foreach (var u in Enumerable.Range(1, 6))
            {
                yield return new UserProfile
                {
                    Id = Guid.NewGuid(),
                    FirstName = f.Name.FirstName(),
                    LastName = f.Name.LastName(),
                    EmailAddress = f.Internet.Email(),
                    IsSystemAdmin = false
                };
            }
        }
    }
}
