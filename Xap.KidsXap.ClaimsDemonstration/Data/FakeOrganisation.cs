﻿using System;
using System.Collections.Generic;
using System.Linq;
using Bogus;
using Xap.KidsXap.ClaimsDemonstration.Domain;

namespace Xap.KidsXap.ClaimsDemonstration.Data
{
    public class FakeOrganisation
    {
        private Config _config;
        private class Config
        {
            public Guid? Id { get; set; }
            public string Name { get; set; }
            public Subscription? Subscription { get; set; }
            public List<Provider> Providers { get; set; }
        }

        public FakeOrganisation()
        {
            _config = new Config();
        }

        public FakeOrganisation WithId(Guid id)
        {
            _config.Id = id;
            return this;
        }

        public FakeOrganisation WithName(string name)
        {
            _config.Name = name;
            return this;
        }

        public FakeOrganisation WithSubscriptionType(Subscription type)
        {
            _config.Subscription = type;
            return this;
        }

        public FakeOrganisation WithProviders(List<Provider> providers)
        {
            _config.Providers = providers;
            return this;
        }

        public Organisation Generate()
        {
            return new Faker<Organisation>()
                .RuleFor(x => x.Id, f => _config.Id ?? Guid.NewGuid())
                .RuleFor(x => x.Name, f => _config.Name ?? f.Company.CompanyName())
                .RuleFor(x => x.Subscription, f => _config.Subscription ?? f.PickRandom<Subscription>())
                .RuleFor(x => x.Providers, (f, u) =>
                {
                    if (_config.Providers != null)
                    {
                        u.AddProviders(_config.Providers);
                    }
                    else
                    {
                        u.AddProviders(new FakeProvider().Generate(3).ToList());
                    }
                    return u.Providers;
                }).Generate();
        }

        public IEnumerable<Organisation> Generate(int count)
        {
            return Enumerable.Range(0, count).Select(i => Generate());
        }

    }

    public class FakeProvider
    {
        private Config _config;
        private class Config
        {
            public Guid? Id { get; set; }
            public Subscription? Subscription { get; set; }
            public string Name { get; set; }
            public Guid? ParentId { get; set; }
            public List<Centre> Centres { get; set; }
        }
        public FakeProvider()
        {
            _config = new Config();
        }

        public FakeProvider WithId(Guid id)
        {
            _config.Id = id;
            return this;
        }

        public FakeProvider WithName(string name)
        {
            _config.Name = name;
            return this;
        }

        public FakeProvider WithParentId(Guid id)
        {
            _config.ParentId = id;
            return this;
        }

        public FakeProvider WithCentres(List<Centre> centres)
        {
            _config.Centres = centres;
            return this;
        }

        public FakeProvider WithSubscription(Subscription sub)
        {
            _config.Subscription = sub;
            return this;
        }

        public Provider Generate()
        {
            return new Faker<Provider>()
                .RuleFor(x => x.Id, f => _config.Id ?? Guid.NewGuid())
                .RuleFor(x => x.Name, f => _config.Name ?? f.Company.CompanyName())
                .RuleFor(x => x.Subscription, f => _config.Subscription)
                .RuleFor(x => x.ParentId, f => _config.ParentId)
                .RuleFor(x => x.Centres, (f, u) =>
                {
                    if (_config.Centres != null)
                    {
                        u.AddCentres(_config.Centres);
                    }
                    else
                    {
                        u.AddCentres(new FakeCentre().Generate(4));
                        u.AddCentres(new []{new FakeCentre().WithSubscription(Subscription.Premium).Generate()});
                        u.AddCentres(new[] { new FakeCentre().WithSubscription(Subscription.Standard).Generate() });
                    }

                    return u.Centres;
                }).Generate();
        }

        public IEnumerable<Provider> Generate(int count)
        {
            return Enumerable.Range(0, count).Select(i => Generate());
        }
    }

    public class FakeCentre
    {
        private Config _config;

        private class Config
        {
            public Guid? Id { get; set; }
            public Subscription? Subscription { get; set; }
            public string Name { get; set; }
            public Guid? ParentId { get; set; }
        }

        public FakeCentre()
        {
            _config = new Config();
        }

        public FakeCentre WithId(Guid id)
        {
            _config.Id = id;
            return this;
        }

        public FakeCentre WithName(string name)
        {
            _config.Name = name;
            return this;
        }

        public FakeCentre WithParentId(Guid id)
        {
            _config.ParentId = id;
            return this;
        }

        public FakeCentre WithSubscription(Subscription sub)
        {
            _config.Subscription = sub;
            return this;
        }

        public Centre Generate()
        {
            return new Faker<Centre>()
                .RuleFor(x => x.Id, f => _config.Id ?? Guid.NewGuid())
                .RuleFor(x => x.ParentId, f => _config.ParentId)
                .RuleFor(x => x.Subscription, f => _config.Subscription)
                .RuleFor(x => x.Name, f => _config.Name ?? f.Company.CompanyName()).Generate();
        }

        public IEnumerable<Centre> Generate(int count)
        {
            return Enumerable.Range(0, count).Select(i => Generate());
        }
    }

}
