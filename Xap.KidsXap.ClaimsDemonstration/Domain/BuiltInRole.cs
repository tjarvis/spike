﻿namespace Xap.KidsXap.ClaimsDemonstration.Domain
{
    public enum BuiltInRole
    {
        OrganisationAdmin,
        ProviderAdmin,
        CentreAdmin,
        Guardian
    }
}
