﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Xap.KidsXap.ClaimsDemonstration.Domain
{
    public static class SubscriptionClaims
    {
        public static IEnumerable<string> ClaimsForSubscription(Subscription subscription)
        {
            switch (subscription)
            {
                case Subscription.Basic: return BasicClaims();
                case Subscription.Standard: return StandardClaims();
                case Subscription.Premium: return PremiumClaims();
                default: return BasicClaims();
            }
        }

        public static IEnumerable<string> BasicClaims()
        {
            // Feature.ManageDashBoard
            yield return Feature.ManageDashBoard.ToFeatureClaim();
            yield return Permission.Dashboard.ToEnterpriseClaim();
            yield return Permission.Dashboard.ToGeneralClaim();
            // Feature.AuditLogs
            yield return Feature.AuditLogs.ToFeatureClaim();
            yield return Permission.AuditLogs.ToEnterpriseClaim();
            yield return Permission.AuditLogs.ToGeneralClaim();
            // Feature.Onboarding
            yield return Feature.Onboarding.ToFeatureClaim();
            yield return Permission.Provider.ToEnterpriseClaim();
            yield return Permission.Provider.ToGeneralClaim();
            yield return Permission.Centre.ToEnterpriseClaim();
            yield return Permission.Centre.ToGeneralClaim();
            // Feature.FamiliesGuardiansChildrenPaymentAccounts
            yield return Feature.FamiliesGuardiansChildrenPaymentAccounts.ToFeatureClaim();
            yield return Permission.Family.ToGeneralClaim();
            // Feature.BookingsAndAttendance
            yield return Feature.BookingsAndAttendance.ToFeatureClaim();
            yield return Permission.BookingAttendances.ToGeneralClaim();
            yield return Permission.Kiosk.ToGeneralClaim();
            // Feature.PaymentsAndBilling
            yield return Feature.PaymentsAndBilling.ToFeatureClaim();
            yield return Permission.Payments.ToGeneralClaim();
            yield return Permission.Transactions.ToGeneralClaim();
            yield return Permission.BulkCharges.ToGeneralClaim();
            yield return Permission.Statements.ToGeneralClaim();
            yield return Permission.Invoices.ToGeneralClaim();
            // Feature.Communications
            yield return Feature.Communications.ToFeatureClaim();
            yield return Permission.NewsLetters.ToGeneralClaim();
            // Feature.CCSServices
            yield return Feature.CcsServices.ToFeatureClaim();
            yield return Permission.Proda.ToGeneralClaim();
            yield return Permission.CcssProvider.ToGeneralClaim();
            yield return Permission.CcssService.ToGeneralClaim();
            yield return Permission.CcssPersonnel.ToGeneralClaim();
            yield return Permission.CcssEnrolmentsEntitlements.ToGeneralClaim();
            yield return Permission.CcssPaymentReports.ToGeneralClaim();
            yield return Permission.CcssSessionReports.ToGeneralClaim();
            yield return Permission.CcssMessages.ToGeneralClaim();
            yield return Permission.CcssDebtManagement.ToGeneralClaim();
            yield return Permission.ReturnFeeReduction.ToGeneralClaim();
            yield return Permission.CareProvidedAndVacancy.ToGeneralClaim();
            yield return Permission.AccsWellbeing.ToGeneralClaim();
            yield return Permission.IsCaseClaims.ToGeneralClaim();
            // Feature.UserManagement
            yield return Feature.UserManagement.ToFeatureClaim();
            yield return Permission.Users.ToEnterpriseClaim();
            yield return Permission.Users.ToGeneralClaim();
            yield return Permission.Roles.ToEnterpriseClaim();
            yield return Permission.Roles.ToGeneralClaim();
            yield return Permission.Groups.ToEnterpriseClaim();
            yield return Permission.Groups.ToGeneralClaim();
            // Feature.Reports
            yield return Feature.Reports.ToFeatureClaim();
            yield return Permission.FinancialReports.ToEnterpriseClaim();
            yield return Permission.FinancialReports.ToGeneralClaim();
            yield return Permission.FamilyReports.ToGeneralClaim();
            yield return Permission.BookingsAttendanceReports.ToGeneralClaim();
            yield return Permission.CcssReports.ToEnterpriseClaim();
            yield return Permission.CcssReports.ToGeneralClaim();
            yield return Permission.MiscellaneousReports.ToEnterpriseClaim();
            yield return Permission.MiscellaneousReports.ToGeneralClaim();
            // Feature.SettingsGeneralConfiguration
            yield return Feature.SettingsGeneralConfiguration.ToFeatureClaim();
            yield return Permission.OrganisationDetails.ToEnterpriseClaim();
            yield return Permission.ProviderDetails.ToEnterpriseClaim();
            yield return Permission.CentreDetails.ToEnterpriseClaim();
            yield return Permission.CentreDetails.ToGeneralClaim();
            yield return Permission.OperationDetails.ToGeneralClaim();
            yield return Permission.PaymentsAndBillingProfile.ToGeneralClaim();
            yield return Permission.DirectDebitProfile.ToGeneralClaim();
            yield return Permission.SessionAndFees.ToGeneralClaim();
            yield return Permission.RoomConfiguration.ToGeneralClaim();
            yield return Permission.TermsAndConditions.ToGeneralClaim();
            yield return Permission.PrivacyPolicyAndDisclaimer.ToGeneralClaim();
            // Feature.SettingsWaitListEnrolmentsAndBookings
            yield return Feature.SettingsWaitListEnrolmentsAndBookings.ToFeatureClaim();
            yield return Permission.WaitlistConfiguration.ToGeneralClaim();
            yield return Permission.BookingConfiguration.ToGeneralClaim();
            yield return Permission.DiscountSetup.ToGeneralClaim();
            // Feature.SettingsUserManagement
            yield return Feature.SettingsUserManagement.ToFeatureClaim();
            yield return Permission.UsersSettings.ToEnterpriseClaim();
            yield return Permission.UsersSettings.ToGeneralClaim();
            yield return Permission.RolesSettings.ToEnterpriseClaim();
            yield return Permission.RolesSettings.ToGeneralClaim();
            yield return Permission.GroupsSettings.ToEnterpriseClaim();
            yield return Permission.Groups.ToGeneralClaim();
            // Feature.SettingsOther
            yield return Feature.SettingsOther.ToFeatureClaim();
            yield return Permission.EmailTemplatesSettings.ToGeneralClaim();
            yield return Permission.Tags.ToGeneralClaim();
        }

        public static IEnumerable<string> StandardClaims()
        {
            // Feature.ManageDashBoard
            yield return Feature.ManageDashBoard.ToFeatureClaim();
            yield return Permission.Dashboard.ToEnterpriseClaim();
            yield return Permission.Dashboard.ToGeneralClaim();
            // Feature.AuditLogs
            yield return Feature.AuditLogs.ToFeatureClaim();
            yield return Permission.AuditLogs.ToEnterpriseClaim();
            yield return Permission.AuditLogs.ToGeneralClaim();
            // Feature.Onboarding
            yield return Feature.Onboarding.ToFeatureClaim();
            yield return Permission.Provider.ToEnterpriseClaim();
            yield return Permission.Provider.ToGeneralClaim();
            yield return Permission.Centre.ToEnterpriseClaim();
            yield return Permission.Centre.ToGeneralClaim();
            // Feature.FamiliesGuardiansChildrenPaymentAccounts
            yield return Feature.FamiliesGuardiansChildrenPaymentAccounts.ToFeatureClaim();
            yield return Permission.Family.ToGeneralClaim();
            yield return Permission.Family.ToEnterpriseClaim();
            yield return Permission.BulkActions.ToEnterpriseClaim();
            yield return Permission.BulkActions.ToGeneralClaim();
            // Feature.BookingsAndAttendance
            yield return Feature.BookingsAndAttendance.ToFeatureClaim();
            yield return Permission.BookingAttendances.ToEnterpriseClaim();
            yield return Permission.BookingAttendances.ToGeneralClaim();
            yield return Permission.Kiosk.ToEnterpriseClaim();
            yield return Permission.Kiosk.ToGeneralClaim();
            yield return Permission.BookingBulkActions.ToEnterpriseClaim();
            yield return Permission.BookingBulkActions.ToGeneralClaim();
            // Feature.PaymentsAndBilling
            yield return Feature.PaymentsAndBilling.ToFeatureClaim();
            yield return Permission.Payments.ToEnterpriseClaim();
            yield return Permission.Payments.ToGeneralClaim();
            yield return Permission.Transactions.ToEnterpriseClaim();
            yield return Permission.Transactions.ToGeneralClaim();
            yield return Permission.BulkCharges.ToEnterpriseClaim();
            yield return Permission.BulkCharges.ToGeneralClaim();
            yield return Permission.Statements.ToEnterpriseClaim();
            yield return Permission.Statements.ToGeneralClaim();
            yield return Permission.Invoices.ToEnterpriseClaim();
            yield return Permission.Invoices.ToGeneralClaim();
            // Feature.Communications
            yield return Feature.Communications.ToFeatureClaim();
            yield return Permission.NewsLetters.ToEnterpriseClaim();
            yield return Permission.NewsLetters.ToGeneralClaim();
            yield return Permission.Activities.ToEnterpriseClaim();
            yield return Permission.Activities.ToGeneralClaim();
            yield return Permission.InAppMessaging.ToEnterpriseClaim();
            yield return Permission.InAppMessaging.ToGeneralClaim();
            yield return Permission.PushNotifications.ToEnterpriseClaim();
            yield return Permission.PushNotifications.ToGeneralClaim();
            yield return Permission.Sms.ToEnterpriseClaim();
            yield return Permission.Sms.ToGeneralClaim();
            // Feature.CCSServices
            yield return Feature.CcsServices.ToFeatureClaim();
            yield return Permission.Proda.ToEnterpriseClaim();
            yield return Permission.Proda.ToGeneralClaim();
            yield return Permission.CcssProvider.ToEnterpriseClaim();
            yield return Permission.CcssProvider.ToGeneralClaim();
            yield return Permission.CcssService.ToEnterpriseClaim();
            yield return Permission.CcssService.ToGeneralClaim();
            yield return Permission.CcssPersonnel.ToEnterpriseClaim();
            yield return Permission.CcssPersonnel.ToGeneralClaim();
            yield return Permission.CcssEnrolmentsEntitlements.ToEnterpriseClaim();
            yield return Permission.CcssEnrolmentsEntitlements.ToGeneralClaim();
            yield return Permission.CcssPaymentReports.ToEnterpriseClaim();
            yield return Permission.CcssPaymentReports.ToGeneralClaim();
            yield return Permission.CcssSessionReports.ToEnterpriseClaim();
            yield return Permission.CcssSessionReports.ToGeneralClaim();
            yield return Permission.CcssMessages.ToEnterpriseClaim();
            yield return Permission.CcssMessages.ToGeneralClaim();
            yield return Permission.CcssDebtManagement.ToEnterpriseClaim();
            yield return Permission.CcssDebtManagement.ToGeneralClaim();
            yield return Permission.ReturnFeeReduction.ToEnterpriseClaim();
            yield return Permission.ReturnFeeReduction.ToGeneralClaim();
            yield return Permission.CareProvidedAndVacancy.ToEnterpriseClaim();
            yield return Permission.CareProvidedAndVacancy.ToGeneralClaim();
            yield return Permission.AccsWellbeing.ToEnterpriseClaim();
            yield return Permission.AccsWellbeing.ToGeneralClaim();
            yield return Permission.IsCaseClaims.ToEnterpriseClaim();
            yield return Permission.IsCaseClaims.ToGeneralClaim();
            // Feature.UserManagement
            yield return Feature.UserManagement.ToFeatureClaim();
            yield return Permission.Users.ToEnterpriseClaim();
            yield return Permission.Users.ToGeneralClaim();
            yield return Permission.Roles.ToEnterpriseClaim();
            yield return Permission.Roles.ToGeneralClaim();
            yield return Permission.Groups.ToEnterpriseClaim();
            yield return Permission.Groups.ToGeneralClaim();
            // Feature.Reports
            yield return Feature.Reports.ToFeatureClaim();
            yield return Permission.FinancialReports.ToEnterpriseClaim();
            yield return Permission.FinancialReports.ToGeneralClaim();
            yield return Permission.FamilyReports.ToEnterpriseClaim();
            yield return Permission.FamilyReports.ToGeneralClaim();
            yield return Permission.BookingsAttendanceReports.ToEnterpriseClaim();
            yield return Permission.BookingsAttendanceReports.ToGeneralClaim();
            yield return Permission.CcssReports.ToEnterpriseClaim();
            yield return Permission.CcssReports.ToGeneralClaim();
            yield return Permission.MiscellaneousReports.ToEnterpriseClaim();
            yield return Permission.MiscellaneousReports.ToGeneralClaim();
            yield return Permission.ActivityReports.ToEnterpriseClaim();
            yield return Permission.ActivityReports.ToGeneralClaim();
            yield return Permission.WaitlistReports.ToEnterpriseClaim();
            yield return Permission.WaitlistReports.ToGeneralClaim();
            // Feature.SettingsGeneralConfiguration
            yield return Feature.SettingsGeneralConfiguration.ToFeatureClaim();
            yield return Permission.OrganisationDetails.ToEnterpriseClaim();
            yield return Permission.ProviderDetails.ToEnterpriseClaim();
            yield return Permission.CentreDetails.ToEnterpriseClaim();
            yield return Permission.CentreDetails.ToGeneralClaim();
            yield return Permission.OperationDetails.ToEnterpriseClaim();
            yield return Permission.OperationDetails.ToGeneralClaim();
            yield return Permission.PaymentsAndBillingProfile.ToEnterpriseClaim();
            yield return Permission.PaymentsAndBillingProfile.ToGeneralClaim();
            yield return Permission.DirectDebitProfile.ToEnterpriseClaim();
            yield return Permission.DirectDebitProfile.ToGeneralClaim();
            yield return Permission.SessionAndFees.ToEnterpriseClaim();
            yield return Permission.SessionAndFees.ToGeneralClaim();
            yield return Permission.RoomConfiguration.ToEnterpriseClaim();
            yield return Permission.RoomConfiguration.ToGeneralClaim();
            yield return Permission.TermsAndConditions.ToEnterpriseClaim();
            yield return Permission.TermsAndConditions.ToGeneralClaim();
            yield return Permission.PrivacyPolicyAndDisclaimer.ToEnterpriseClaim();
            yield return Permission.PrivacyPolicyAndDisclaimer.ToGeneralClaim();
            // Feature.SettingsWaitListEnrolmentsAndBookings
            yield return Feature.SettingsWaitListEnrolmentsAndBookings.ToFeatureClaim();
            yield return Permission.WaitlistConfiguration.ToEnterpriseClaim();
            yield return Permission.WaitlistConfiguration.ToGeneralClaim();
            yield return Permission.BookingConfiguration.ToEnterpriseClaim();
            yield return Permission.BookingConfiguration.ToGeneralClaim();
            yield return Permission.DiscountSetup.ToEnterpriseClaim();
            yield return Permission.DiscountSetup.ToGeneralClaim();
            yield return Permission.EnrolmentFormConfiguration.ToEnterpriseClaim();
            yield return Permission.EnrolmentFormConfiguration.ToGeneralClaim();
            // Feature.SettingsOther
            yield return Feature.SettingsOther.ToFeatureClaim();
            yield return Permission.EmailTemplatesSettings.ToEnterpriseClaim();
            yield return Permission.EmailTemplatesSettings.ToGeneralClaim();
            yield return Permission.Tags.ToEnterpriseClaim();
            yield return Permission.Tags.ToGeneralClaim();
            // Feature.SettingsLearningAndPrograms
            yield return Feature.SettingsLearningAndPrograms.ToFeatureClaim();
            yield return Permission.LearningObservationsSettings.ToEnterpriseClaim();
            yield return Permission.LearningObservationsSettings.ToGeneralClaim();
            yield return Permission.ProgramPlanSettings.ToEnterpriseClaim();
            yield return Permission.ProgramPlanSettings.ToGeneralClaim();
            // Feature.SettingsHealthAndMedications
            yield return Feature.SettingsHealthAndMedications.ToFeatureClaim();
            yield return Permission.IncidentRecordsSettings.ToEnterpriseClaim();
            yield return Permission.IncidentRecordsSettings.ToGeneralClaim();
            yield return Permission.MedicationRequestsSettings.ToEnterpriseClaim();
            yield return Permission.MedicationRequestsSettings.ToGeneralClaim();
            yield return Permission.RosterSettings.ToEnterpriseClaim();
            yield return Permission.RosterSettings.ToGeneralClaim();
            // Feature.SettingsDocuments
            yield return Feature.SettingsDocuments.ToFeatureClaim();
            yield return Permission.QualityImprovementPlan.ToEnterpriseClaim();
            yield return Permission.QualityImprovementPlan.ToGeneralClaim();
            yield return Permission.TransitionPlan.ToEnterpriseClaim();
            yield return Permission.TransitionPlan.ToGeneralClaim();
            yield return Permission.CentrePoliciesAndDocuments.ToEnterpriseClaim();
            yield return Permission.CentrePoliciesAndDocuments.ToGeneralClaim();
            // Feature.SettingsRostering
            yield return Feature.SettingsRostering.ToFeatureClaim();
            yield return Permission.StaffRoster.ToEnterpriseClaim();
            yield return Permission.StaffRoster.ToGeneralClaim();
            // Feature.APILogs
            yield return Feature.ApiLogs.ToFeatureClaim();
            yield return Permission.CcssApilogs.ToEnterpriseClaim();
            yield return Permission.CcssApilogs.ToGeneralClaim();
            // Feature.WaitList
            yield return Feature.WaitList.ToFeatureClaim();
            yield return Permission.Waitlist.ToEnterpriseClaim();
            yield return Permission.Waitlist.ToGeneralClaim();
            // Feature.LearningAndPrograms
            yield return Feature.LearningAndPrograms.ToFeatureClaim();
            yield return Permission.LearningObservations.ToEnterpriseClaim();
            yield return Permission.LearningObservations.ToGeneralClaim();
            yield return Permission.ProgramPlan.ToEnterpriseClaim();
            yield return Permission.ProgramPlan.ToGeneralClaim();
            // Feature.Rostering
            yield return Feature.Rostering.ToFeatureClaim();
            yield return Permission.StaffRoster.ToEnterpriseClaim();
            yield return Permission.StaffRoster.ToGeneralClaim();
            // Feature.HealthAndMedications
            yield return Feature.HealthAndMedications.ToFeatureClaim();
            yield return Permission.IncidentRecords.ToEnterpriseClaim();
            yield return Permission.IncidentRecords.ToGeneralClaim();
            yield return Permission.MedicationRequests.ToEnterpriseClaim();
            yield return Permission.MedicationRequests.ToGeneralClaim();
            // Feature.Documents
            yield return Feature.Documents.ToFeatureClaim();
            yield return Permission.QualityImprovementPlan.ToEnterpriseClaim();
            yield return Permission.QualityImprovementPlan.ToGeneralClaim();
            yield return Permission.TransitionPlan.ToEnterpriseClaim();
            yield return Permission.TransitionPlan.ToGeneralClaim();
            yield return Permission.CentrePoliciesAndDocuments.ToEnterpriseClaim();
            yield return Permission.CentrePoliciesAndDocuments.ToGeneralClaim();
        }

        public static IEnumerable<string> PremiumClaims()
        {
            // Feature.ManageDashBoard
            yield return Feature.ManageDashBoard.ToFeatureClaim();
            yield return Permission.Dashboard.ToEnterpriseClaim();
            yield return Permission.Dashboard.ToGeneralClaim();
            // Feature.AuditLogs
            yield return Feature.AuditLogs.ToFeatureClaim();
            yield return Permission.AuditLogs.ToEnterpriseClaim();
            yield return Permission.AuditLogs.ToGeneralClaim();
            // Feature.Onboarding
            yield return Feature.Onboarding.ToFeatureClaim();
            yield return Permission.Provider.ToEnterpriseClaim();
            yield return Permission.Provider.ToGeneralClaim();
            yield return Permission.Centre.ToEnterpriseClaim();
            yield return Permission.Centre.ToGeneralClaim();
            // Feature.FamiliesGuardiansChildrenPaymentAccounts
            yield return Feature.FamiliesGuardiansChildrenPaymentAccounts.ToFeatureClaim();
            yield return Permission.Family.ToGeneralClaim();
            yield return Permission.Family.ToEnterpriseClaim();
            yield return Permission.BulkActions.ToEnterpriseClaim();
            yield return Permission.BulkActions.ToGeneralClaim();
            // Feature.BookingsAndAttendance
            yield return Feature.BookingsAndAttendance.ToFeatureClaim();
            yield return Permission.BookingAttendances.ToEnterpriseClaim();
            yield return Permission.BookingAttendances.ToGeneralClaim();
            yield return Permission.Kiosk.ToEnterpriseClaim();
            yield return Permission.Kiosk.ToGeneralClaim();
            yield return Permission.BookingBulkActions.ToEnterpriseClaim();
            yield return Permission.BookingBulkActions.ToGeneralClaim();
            // Feature.PaymentsAndBilling
            yield return Feature.PaymentsAndBilling.ToFeatureClaim();
            yield return Permission.Payments.ToEnterpriseClaim();
            yield return Permission.Payments.ToGeneralClaim();
            yield return Permission.Transactions.ToEnterpriseClaim();
            yield return Permission.Transactions.ToGeneralClaim();
            yield return Permission.BulkCharges.ToEnterpriseClaim();
            yield return Permission.BulkCharges.ToGeneralClaim();
            yield return Permission.Statements.ToEnterpriseClaim();
            yield return Permission.Statements.ToGeneralClaim();
            yield return Permission.Invoices.ToEnterpriseClaim();
            yield return Permission.Invoices.ToGeneralClaim();
            // Feature.Communications
            yield return Feature.Communications.ToFeatureClaim();
            yield return Permission.NewsLetters.ToEnterpriseClaim();
            yield return Permission.NewsLetters.ToGeneralClaim();
            yield return Permission.Activities.ToEnterpriseClaim();
            yield return Permission.Activities.ToGeneralClaim();
            yield return Permission.InAppMessaging.ToEnterpriseClaim();
            yield return Permission.InAppMessaging.ToGeneralClaim();
            yield return Permission.PushNotifications.ToEnterpriseClaim();
            yield return Permission.PushNotifications.ToGeneralClaim();
            yield return Permission.Sms.ToEnterpriseClaim();
            yield return Permission.Sms.ToGeneralClaim();
            // Feature.CCSServices
            yield return Feature.CcsServices.ToFeatureClaim();
            yield return Permission.Proda.ToEnterpriseClaim();
            yield return Permission.Proda.ToGeneralClaim();
            yield return Permission.CcssProvider.ToEnterpriseClaim();
            yield return Permission.CcssProvider.ToGeneralClaim();
            yield return Permission.CcssService.ToEnterpriseClaim();
            yield return Permission.CcssService.ToGeneralClaim();
            yield return Permission.CcssPersonnel.ToEnterpriseClaim();
            yield return Permission.CcssPersonnel.ToGeneralClaim();
            yield return Permission.CcssEnrolmentsEntitlements.ToEnterpriseClaim();
            yield return Permission.CcssEnrolmentsEntitlements.ToGeneralClaim();
            yield return Permission.CcssPaymentReports.ToEnterpriseClaim();
            yield return Permission.CcssPaymentReports.ToGeneralClaim();
            yield return Permission.CcssSessionReports.ToEnterpriseClaim();
            yield return Permission.CcssSessionReports.ToGeneralClaim();
            yield return Permission.CcssMessages.ToEnterpriseClaim();
            yield return Permission.CcssMessages.ToGeneralClaim();
            yield return Permission.CcssDebtManagement.ToEnterpriseClaim();
            yield return Permission.CcssDebtManagement.ToGeneralClaim();
            yield return Permission.ReturnFeeReduction.ToEnterpriseClaim();
            yield return Permission.ReturnFeeReduction.ToGeneralClaim();
            yield return Permission.CareProvidedAndVacancy.ToEnterpriseClaim();
            yield return Permission.CareProvidedAndVacancy.ToGeneralClaim();
            yield return Permission.AccsWellbeing.ToEnterpriseClaim();
            yield return Permission.AccsWellbeing.ToGeneralClaim();
            yield return Permission.IsCaseClaims.ToEnterpriseClaim();
            yield return Permission.IsCaseClaims.ToGeneralClaim();
            // Feature.UserManagement
            yield return Feature.UserManagement.ToFeatureClaim();
            yield return Permission.Users.ToEnterpriseClaim();
            yield return Permission.Users.ToGeneralClaim();
            yield return Permission.Roles.ToEnterpriseClaim();
            yield return Permission.Roles.ToGeneralClaim();
            yield return Permission.Groups.ToEnterpriseClaim();
            yield return Permission.Groups.ToGeneralClaim();
            // Feature.Reports
            yield return Feature.Reports.ToFeatureClaim();
            yield return Permission.FinancialReports.ToEnterpriseClaim();
            yield return Permission.FinancialReports.ToGeneralClaim();
            yield return Permission.FamilyReports.ToEnterpriseClaim();
            yield return Permission.FamilyReports.ToGeneralClaim();
            yield return Permission.BookingsAttendanceReports.ToEnterpriseClaim();
            yield return Permission.BookingsAttendanceReports.ToGeneralClaim();
            yield return Permission.CcssReports.ToEnterpriseClaim();
            yield return Permission.CcssReports.ToGeneralClaim();
            yield return Permission.MiscellaneousReports.ToEnterpriseClaim();
            yield return Permission.MiscellaneousReports.ToGeneralClaim();
            yield return Permission.ActivityReports.ToEnterpriseClaim();
            yield return Permission.ActivityReports.ToGeneralClaim();
            yield return Permission.WaitlistReports.ToEnterpriseClaim();
            yield return Permission.WaitlistReports.ToGeneralClaim();
            yield return Permission.HealthMedicalReports.ToEnterpriseClaim();
            yield return Permission.HealthMedicalReports.ToGeneralClaim();
            yield return Permission.LearningAndProgrammingReports.ToEnterpriseClaim();
            yield return Permission.LearningAndProgrammingReports.ToGeneralClaim();
            yield return Permission.RosteringReports.ToEnterpriseClaim();
            yield return Permission.RosteringReports.ToGeneralClaim();
            // Feature.SettingsGeneralConfiguration
            yield return Feature.SettingsGeneralConfiguration.ToFeatureClaim();
            yield return Permission.OrganisationDetails.ToEnterpriseClaim();
            yield return Permission.ProviderDetails.ToEnterpriseClaim();
            yield return Permission.CentreDetails.ToEnterpriseClaim();
            yield return Permission.CentreDetails.ToGeneralClaim();
            yield return Permission.OperationDetails.ToEnterpriseClaim();
            yield return Permission.OperationDetails.ToGeneralClaim();
            yield return Permission.PaymentsAndBillingProfile.ToEnterpriseClaim();
            yield return Permission.PaymentsAndBillingProfile.ToGeneralClaim();
            yield return Permission.DirectDebitProfile.ToEnterpriseClaim();
            yield return Permission.DirectDebitProfile.ToGeneralClaim();
            yield return Permission.SessionAndFees.ToEnterpriseClaim();
            yield return Permission.SessionAndFees.ToGeneralClaim();
            yield return Permission.RoomConfiguration.ToEnterpriseClaim();
            yield return Permission.RoomConfiguration.ToGeneralClaim();
            yield return Permission.TermsAndConditions.ToEnterpriseClaim();
            yield return Permission.TermsAndConditions.ToGeneralClaim();
            yield return Permission.PrivacyPolicyAndDisclaimer.ToEnterpriseClaim();
            yield return Permission.PrivacyPolicyAndDisclaimer.ToGeneralClaim();
            // Feature.SettingsWaitListEnrolmentsAndBookings
            yield return Feature.SettingsWaitListEnrolmentsAndBookings.ToFeatureClaim();
            yield return Permission.WaitlistConfiguration.ToEnterpriseClaim();
            yield return Permission.WaitlistConfiguration.ToGeneralClaim();
            yield return Permission.BookingConfiguration.ToEnterpriseClaim();
            yield return Permission.BookingConfiguration.ToGeneralClaim();
            yield return Permission.DiscountSetup.ToEnterpriseClaim();
            yield return Permission.DiscountSetup.ToGeneralClaim();
            yield return Permission.EnrolmentFormConfiguration.ToEnterpriseClaim();
            yield return Permission.EnrolmentFormConfiguration.ToGeneralClaim();
            // Feature.SettingsOther
            yield return Feature.SettingsOther.ToFeatureClaim();
            yield return Permission.EmailTemplatesSettings.ToEnterpriseClaim();
            yield return Permission.EmailTemplatesSettings.ToGeneralClaim();
            yield return Permission.Tags.ToEnterpriseClaim();
            yield return Permission.Tags.ToGeneralClaim();
            yield return Permission.Checklists.ToEnterpriseClaim();
            yield return Permission.Checklists.ToGeneralClaim();
            // Feature.SettingsLearningAndPrograms
            yield return Feature.SettingsLearningAndPrograms.ToFeatureClaim();
            yield return Permission.LearningObservationsSettings.ToEnterpriseClaim();
            yield return Permission.LearningObservationsSettings.ToGeneralClaim();
            yield return Permission.ProgramPlanSettings.ToEnterpriseClaim();
            yield return Permission.ProgramPlanSettings.ToGeneralClaim();
            yield return Permission.WeeklyFoodMenuSettings.ToEnterpriseClaim();
            yield return Permission.WeeklyFoodMenuSettings.ToGeneralClaim();
            yield return Permission.DailyJournalSettings.ToEnterpriseClaim();
            yield return Permission.DailyJournalSettings.ToGeneralClaim();
            yield return Permission.PortfolioSettings.ToEnterpriseClaim();
            yield return Permission.PortfolioSettings.ToGeneralClaim();
            // Feature.SettingsHealthAndMedications
            yield return Feature.SettingsHealthAndMedications.ToFeatureClaim();
            yield return Permission.IncidentRecordsSettings.ToEnterpriseClaim();
            yield return Permission.IncidentRecordsSettings.ToGeneralClaim();
            yield return Permission.MedicationRequestsSettings.ToEnterpriseClaim();
            yield return Permission.MedicationRequestsSettings.ToGeneralClaim();
            yield return Permission.RosterSettings.ToEnterpriseClaim();
            yield return Permission.RosterSettings.ToGeneralClaim();
            yield return Permission.MedicalManagementPlansSettings.ToEnterpriseClaim();
            yield return Permission.MedicalManagementPlansSettings.ToGeneralClaim();
            // Feature.SettingsDocuments
            yield return Feature.SettingsDocuments.ToFeatureClaim();
            yield return Permission.QualityImprovementPlan.ToEnterpriseClaim();
            yield return Permission.QualityImprovementPlan.ToGeneralClaim();
            yield return Permission.TransitionPlan.ToEnterpriseClaim();
            yield return Permission.TransitionPlan.ToGeneralClaim();
            yield return Permission.CentrePoliciesAndDocuments.ToEnterpriseClaim();
            yield return Permission.CentrePoliciesAndDocuments.ToGeneralClaim();
            // Feature.SettingsRostering
            yield return Feature.SettingsRostering.ToFeatureClaim();
            yield return Permission.StaffRoster.ToEnterpriseClaim();
            yield return Permission.StaffRoster.ToGeneralClaim();
            yield return Permission.Timesheets.ToEnterpriseClaim();
            yield return Permission.Timesheets.ToGeneralClaim();
            yield return Permission.StaffCheckInAndCheckOut.ToEnterpriseClaim();
            yield return Permission.StaffCheckInAndCheckOut.ToGeneralClaim();
            yield return Permission.LeaveManagementSettings.ToEnterpriseClaim();
            yield return Permission.LeaveManagementSettings.ToGeneralClaim();
            // Feature.AuditLogs
            yield return Feature.AuditLogs.ToFeatureClaim();
            yield return Permission.AuditLogs.ToEnterpriseClaim();
            yield return Permission.AuditLogs.ToGeneralClaim();
            // Feature.APILogs
            yield return Feature.ApiLogs.ToFeatureClaim();
            yield return Permission.CcssApilogs.ToEnterpriseClaim();
            yield return Permission.CcssApilogs.ToGeneralClaim();
            yield return Permission.SendGridApiLogs.ToEnterpriseClaim();
            yield return Permission.SendGridApiLogs.ToGeneralClaim();
            yield return Permission.PaymentGatewayApiLogs.ToEnterpriseClaim();
            yield return Permission.PaymentGatewayApiLogs.ToGeneralClaim();
            // Feature.WaitList
            yield return Feature.WaitList.ToFeatureClaim();
            yield return Permission.Waitlist.ToEnterpriseClaim();
            yield return Permission.Waitlist.ToGeneralClaim();
            // Feature.LearningAndPrograms
            yield return Feature.LearningAndPrograms.ToFeatureClaim();
            yield return Permission.LearningObservations.ToEnterpriseClaim();
            yield return Permission.LearningObservations.ToGeneralClaim();
            yield return Permission.ProgramPlan.ToEnterpriseClaim();
            yield return Permission.ProgramPlan.ToGeneralClaim();
            yield return Permission.WeeklyFoodMenu.ToEnterpriseClaim();
            yield return Permission.WeeklyFoodMenu.ToGeneralClaim();
            yield return Permission.DailyJournal.ToEnterpriseClaim();
            yield return Permission.DailyJournal.ToGeneralClaim();
            yield return Permission.Portfolio.ToEnterpriseClaim();
            yield return Permission.Portfolio.ToGeneralClaim();
            // Feature.Rostering
            yield return Feature.Rostering.ToFeatureClaim();
            yield return Permission.StaffRoster.ToEnterpriseClaim();
            yield return Permission.StaffRoster.ToGeneralClaim();
            yield return Permission.Timesheets.ToEnterpriseClaim();
            yield return Permission.Timesheets.ToGeneralClaim();
            yield return Permission.StaffCheckInAndCheckOut.ToEnterpriseClaim();
            yield return Permission.StaffCheckInAndCheckOut.ToGeneralClaim();
            yield return Permission.LeaveManagement.ToEnterpriseClaim();
            yield return Permission.LeaveManagement.ToGeneralClaim();
            // Feature.HealthAndMedications
            yield return Feature.HealthAndMedications.ToFeatureClaim();
            yield return Permission.IncidentRecords.ToEnterpriseClaim();
            yield return Permission.IncidentRecords.ToGeneralClaim();
            yield return Permission.MedicationRequests.ToEnterpriseClaim();
            yield return Permission.MedicationRequests.ToGeneralClaim();
            yield return Permission.MedicalManagementPlans.ToEnterpriseClaim();
            yield return Permission.MedicalManagementPlans.ToGeneralClaim();
            yield return Permission.DayToDayCheckList.ToEnterpriseClaim();
            yield return Permission.DayToDayCheckList.ToGeneralClaim();
            yield return Permission.HealthMedicalCheckLists.ToEnterpriseClaim();
            yield return Permission.HealthMedicalCheckLists.ToGeneralClaim();
            // Feature.Documents
            yield return Feature.Documents.ToFeatureClaim();
            yield return Permission.QualityImprovementPlan.ToEnterpriseClaim();
            yield return Permission.QualityImprovementPlan.ToGeneralClaim();
            yield return Permission.TransitionPlan.ToEnterpriseClaim();
            yield return Permission.TransitionPlan.ToGeneralClaim();
            yield return Permission.CentrePoliciesAndDocuments.ToEnterpriseClaim();
            yield return Permission.CentrePoliciesAndDocuments.ToGeneralClaim();
            // Feature.AdvancedReporting
            yield return Feature.AdvancedReporting.ToFeatureClaim();
            yield return Permission.AdvancedReporting.ToEnterpriseClaim();
            yield return Permission.AdvancedReporting.ToGeneralClaim();
            // Feature.Visitors
            yield return Feature.Visitors.ToFeatureClaim();
            yield return Permission.Vistors.ToEnterpriseClaim();
            yield return Permission.Vistors.ToGeneralClaim();
        }

        public static IEnumerable<Permission> ExtractPermissions(this IEnumerable<string> claims)
        {
            var permissions = new HashSet<Permission>();
            foreach (var c in claims.Where(c => c.StartsWith("enterprise/") || c.StartsWith("general/")))
            {
                var name = c.Substring(c.LastIndexOf('/') + 1);
                var result = Enum.TryParse(name, true, out Permission p);
                if (result)
                {
                    permissions.Add(p);
                }
            }

            return permissions;
        }
    }

}
