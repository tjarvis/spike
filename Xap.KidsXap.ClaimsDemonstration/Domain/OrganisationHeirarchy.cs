﻿using System;
using System.Collections.Generic;

namespace Xap.KidsXap.ClaimsDemonstration.Domain
{
    public class Organisation 
    {
        public Organisation()
        {
            Providers = new List<Provider>();
        }

        public Guid Id { get; set; }

        public Subscription Subscription { get; set; }

        public string Name { get; set; }

        public void AddProviders(IEnumerable<Provider> providers)
        {
            foreach (var p in providers)
            {
                p.ParentId = Id;
                Providers.Add(p);
            }
        }

        public ICollection<Provider> Providers { get; set; }

        public override string ToString()
        {
            return $"{Name} ({Subscription})";
        }
    }

    public class Provider 
    {
        public Provider()
        {
            Centres = new List<Centre>();
        }

        public Guid Id { get; set; }
        public Subscription? Subscription { get; set; }
        public string Name { get; set; }
        public Guid? ParentId { get; set; }

        public void AddCentres(IEnumerable<Centre> centres)
        {
            foreach (var c in centres)
            {
                if (!c.Subscription.HasValue)
                {
                    c.Subscription = Subscription;
                }
                c.ParentId = Id;
                Centres.Add(c);
            }
        }
        public ICollection<Centre> Centres { get; set; }

        public override string ToString()
        {
            return Subscription.HasValue ? $"{Name} ({Subscription})" : Name;
        }
    }

    public class Centre 
    {
        public Guid Id { get; set; }
        public Subscription? Subscription { get; set; }
        public string Name { get; set; }
        public Guid? ParentId { get; set; }

        public override string ToString()
        {
            return Subscription.HasValue ? $"{Name} ({Subscription})" : Name;
        }
    }

}
