﻿namespace Xap.KidsXap.ClaimsDemonstration.Domain
{
    public enum SystemRole
    {
        OrganisationAdmin,
        ProviderAdmin,
        CentreAdmin,
        Guardian
    }
}
