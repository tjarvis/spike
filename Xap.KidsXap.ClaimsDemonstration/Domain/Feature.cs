﻿namespace Xap.KidsXap.ClaimsDemonstration.Domain
{
    public enum Feature
    {
        AdvancedReporting,
        ApiLogs,
        AuditLogs,
        BookingsAndAttendance,
        CcsServices,
        Communications,
        Documents,
        FamiliesGuardiansChildrenPaymentAccounts,
        HealthAndMedications,
        LearningAndPrograms,
        ManageDashBoard,
        Onboarding,
        PaymentsAndBilling,
        Reports,
        Rostering,
        SettingsDocuments,
        SettingsGeneralConfiguration,
        SettingsHealthAndMedications,
        SettingsLearningAndPrograms,
        SettingsOther,
        SettingsRostering,
        SettingsUserManagement,
        SettingsWaitListEnrolmentsAndBookings,
        UserManagement,
        Visitors,
        WaitList
    }
}

