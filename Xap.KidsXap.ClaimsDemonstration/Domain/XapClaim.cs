﻿using System;

namespace Xap.KidsXap.ClaimsDemonstration.Domain
{
    public class XapClaim
    {
        public string Claim { get; set; }
        public Guid? TenantId { get; set; }
        public bool? WildCardClaim { get; set; }  // System Admin's only.
    }
}
