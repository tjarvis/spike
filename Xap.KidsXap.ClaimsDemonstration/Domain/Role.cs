﻿using System;
using System.Collections.Generic;
using System.Linq;


namespace Xap.KidsXap.ClaimsDemonstration.Domain
{
    public class Role
    {
        public Role()
        {
            Permissions = new HashSet<string>();
            Tenants = new HashSet<Guid>();
        }

        public Guid Id { get; set; }
        public HashSet<Guid> Tenants { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public SystemRole? SystemRole { get; set; }
        public HashSet<string> Permissions { get; set; }

        public override string ToString()
        {
            return Name;
        }
    }

    public static class RoleExtensions
    {
        public static IEnumerable<Permission> ExtractPermissionsFromPermissionClaims(this IEnumerable<string> claims)
        {
            var permissions = new HashSet<Permission>();
            foreach (var c in claims.Where(c => c.StartsWith("permission/")))
            {
                var name = c.Substring(c.LastIndexOf('/') + 1);
                var result = Enum.TryParse(name, true, out Permission p);
                if (result)
                {
                    permissions.Add(p);
                }
            }

            return permissions;
        }
    }

}
