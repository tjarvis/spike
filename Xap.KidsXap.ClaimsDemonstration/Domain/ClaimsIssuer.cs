﻿using System;
using System.Collections.Generic;
using System.Linq;
using Xap.KidsXap.ClaimsDemonstration.Data;

namespace Xap.KidsXap.ClaimsDemonstration.Domain
{
    public class ClaimsIssuer
    {
        public IEnumerable<XapClaim> ClaimsForUser(UserProfile user)
        {
            if (user.IsSystemAdmin)
            {
                return SystemAdminClaims();
            }

            HashSet<XapClaim> claims = new HashSet<XapClaim>();
            foreach (var r in user.Roles)
            {
                var role = Repository.Roles.SingleOrDefault(x => x.Id == r.RoleId);
                if(role == null) throw new Exception("Oops - That Role is not in the repo.");
                var roleClaim = $"role/{role.Name.Replace(" ", "").ToLower()}";
                var tenantSubscriptions = Repository.GetTenantSubscriptions().Where(x => r.TenantIds.Contains(x.Id));
                foreach (var sub in tenantSubscriptions)
                {
                    claims.Add(new XapClaim {Claim = roleClaim, TenantId = sub.Id});
                    var featureClaims = SubscriptionClaims.ClaimsForSubscription(sub.Subscription ?? Subscription.Basic).ToList();

                    // Add all Top Level Features for Subscription
                    foreach (var f in featureClaims.Where(x => x.StartsWith("feature")))
                    {
                        claims.Add(new XapClaim {Claim = f, TenantId = sub.Id});
                    }

                    // Only add the Sub-Features if we have the permissions for them.
                    var subFeaturePermissions = featureClaims.ExtractPermissions();
                    var rolePermissions = role.Permissions.ExtractPermissionsFromPermissionClaims();

                    var commonPermissions = subFeaturePermissions.Intersect(rolePermissions);

                    foreach (var p in commonPermissions)
                    {
                        if (role.Permissions.Contains(p.ToReadClaim()))
                        {
                            claims.Add(new XapClaim {Claim = p.ToReadClaim(), TenantId = sub.Id});
                        }

                        if (role.Permissions.Contains(p.ToManageClaim()))
                        {
                            claims.Add(new XapClaim { Claim = p.ToManageClaim(), TenantId = sub.Id });
                        }

                        if(featureClaims.Contains(p.ToGeneralClaim()))
                        {
                            claims.Add(new XapClaim {Claim = p.ToGeneralClaim(), TenantId = sub.Id});
                        }

                        if (featureClaims.Contains(p.ToEnterpriseClaim()) && role.Permissions.Contains(p.ToEnterprisePermissionClaim()))
                        {
                            claims.Add(new XapClaim {Claim = p.ToEnterpriseClaim(), TenantId = sub.Id});
                        }
                    }
                }
            }
            return claims;
        }

        private IEnumerable<XapClaim> SystemAdminClaims()
        {
            // You get the lot !
            yield return new XapClaim { Claim = Claims.SystemAdmin, WildCardClaim = true };
            foreach (var p in Enum.GetValues(typeof(Permission)).Cast<Permission>())
            {
                yield return new XapClaim { Claim = p.ToManageClaim(), WildCardClaim = true};
                yield return new XapClaim { Claim = p.ToReadClaim(), WildCardClaim = true };
                yield return new XapClaim {Claim = p.ToEnterprisePermissionClaim(), WildCardClaim = true};
                yield return new XapClaim { Claim = p.ToGeneralClaim(), WildCardClaim = true };
                yield return new XapClaim { Claim = p.ToEnterpriseClaim(), WildCardClaim = true };
            }

            foreach (var f in Enum.GetValues(typeof(Feature)).Cast<Feature>())
            {
                yield return new XapClaim { Claim = f.ToFeatureClaim(), WildCardClaim = true };
            }

        }
    }
}
