﻿using System;
using System.Collections.Generic;

namespace Xap.KidsXap.ClaimsDemonstration.Domain
{
    public class UserProfile
    {
        public UserProfile()
        {
            Roles = new List<RoleAssignment>();
        }
        public Guid Id { get; set; }
        public string FirstName { get; set; }
        public string MiddleNames { get; set; }
        public string LastName { get; set; }
        public string EmailAddress { get; set; }
        public bool IsSystemAdmin { get; set; }
        public List<RoleAssignment> Roles { get; set; }
        public override string ToString()
        {
            return IsSystemAdmin ? $"{FirstName} {LastName} (system admin)" : $"{FirstName} {LastName}";
        }
    }

}
