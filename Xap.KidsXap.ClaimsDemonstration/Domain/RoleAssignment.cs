﻿using System;
using System.Collections.Generic;

namespace Xap.KidsXap.ClaimsDemonstration.Domain
{
    public class RoleAssignment
    {
        public RoleAssignment()
        {
            TenantIds = new List<Guid>();
        }
        public Guid RoleId { get; set; }
        public List<Guid> TenantIds { get; set; } 
    }
}
