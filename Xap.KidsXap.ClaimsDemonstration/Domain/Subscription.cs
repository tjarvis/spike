﻿namespace Xap.KidsXap.ClaimsDemonstration.Domain
{
    public enum Subscription
    {
        Basic,
        Standard,
        Premium
    }
}
