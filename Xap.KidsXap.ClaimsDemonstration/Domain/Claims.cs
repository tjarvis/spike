﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Xap.KidsXap.ClaimsDemonstration.Domain
{
    public static class Claims
    {
        private static class Constants
        {
            public const string Permission = "permission";
            public const string Feature = "feature";
            public const string Role = "role";
            public const string ReadAccess = "read";
            public const string ManageAccess = "manage";
            public const string Enterprise = "enterprise"; // Organisation or Provider context
            public const string General = "general"; // Centres Context
        }

        // God-like power.
        public static readonly string SystemAdmin = "role/systemadmin";

        public static string ToReadClaim(this Permission permission)
        {
            return $"{Constants.Permission}/{Constants.ReadAccess}/{permission.ToString().ToLower()}";
        }

        public static string ToManageClaim(this Permission permission)
        {
            return $"{Constants.Permission}/{Constants.ManageAccess}/{permission.ToString().ToLower()}";
        }

        public static string ToEnterprisePermissionClaim(this Permission permission)
        {
            return $"{Constants.Permission}/{Constants.Enterprise}/{permission.ToString().ToLower()}";
        }

        public static string ToEnterpriseClaim(this Permission permission)
        {
            return $"{Constants.Enterprise}/{permission.ToString().ToLower()}";
        }
        public static string ToGeneralClaim(this Permission permission)
        {
            return $"{Constants.General}/{permission.ToString().ToLower()}";
        }

        public static Permission ToPermissionFromClaim(this string claim)
        {
            if (claim.StartsWith($"{Constants.Permission}") || claim.StartsWith($"{Constants.General}") || claim.StartsWith($"{Constants.Enterprise}"))
            {
                var txt = claim.Substring(claim.LastIndexOf("/", StringComparison.Ordinal) + 1);

                if (Enum.TryParse(value: txt, ignoreCase: true, result: out Permission result))
                {
                    return result;
                }

                throw new Exception("Unsupported.");
            }
            throw new Exception("This extension method is only for permission claims");
        }

        public static string ToRoleClaim(this BuiltInRole role)
        {
            return $"{Constants.Role}/{role.ToString().ToLower()}";
        }

        public static string ToFeatureClaim(this Feature feature)
        {
            return $"{Constants.Feature}/{feature.ToString().ToLower()}";
        }

        public static IEnumerable<string> GetPermissionClaims(this IEnumerable<Permission> permissions)
        {
            foreach (var p in permissions)
            {
                yield return p.ToManageClaim();
                yield return p.ToReadClaim();
            }
        }
        public static IEnumerable<string> GetAllClaims()
        {
            yield return SystemAdmin;
            foreach (var p in Enum.GetValues(typeof(Permission)).Cast<Permission>())
            {
                yield return p.ToReadClaim();
                yield return p.ToManageClaim();
                yield return p.ToEnterprisePermissionClaim();
                yield return p.ToGeneralClaim();
                yield return p.ToEnterpriseClaim();
            }

            foreach (var f in Enum.GetValues(typeof(Feature)).Cast<Feature>())
            {
                yield return f.ToFeatureClaim();
            }

            foreach (var r in Enum.GetValues(typeof(BuiltInRole)).Cast<BuiltInRole>())
            {
                yield return r.ToRoleClaim();
            }
        }
    }
}
