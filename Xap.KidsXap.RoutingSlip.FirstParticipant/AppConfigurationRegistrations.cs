﻿using Autofac;
using Microsoft.Extensions.Configuration;
using Serilog;

namespace Xap.KidsXap.RoutingSlip.FirstParticipant
{
    public class AppConfigurationRegistrations : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            var config = new ConfigurationBuilder()
                .AddJsonFile("appsettings.json")
                .Build();

            builder.Register(context => config).As<IConfiguration>();

            builder.Register<ILogger>(context => new LoggerConfiguration()
                .WriteTo.Console()
                .CreateLogger()).SingleInstance();

        }
    }
}
