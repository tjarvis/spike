﻿using System;
using Autofac;

namespace Xap.KidsXap.RoutingSlip.FirstParticipant
{
    public static class FirstParticipantLifeTimeScope
    {
        public static Func<ILifetimeScope> GetLifeTimeScope { get; set; }
    }

}
