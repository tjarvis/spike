﻿using System;
using Autofac;
using MassTransit;
using MassTransit.AutofacIntegration;
using MassTransit.Courier;
using Microsoft.Extensions.Configuration;
using Serilog;
using Xap.KidsXap.RoutingSlip.Common;

namespace Xap.KidsXap.RoutingSlip.FirstParticipant
{
    public class MassTransitRegistration : Module
    {
        protected override void Load(ContainerBuilder builder)
        {

            builder.RegisterGeneric(typeof(AutofacExecuteActivityFactory<,>))
                .WithParameter(new NamedParameter("name", "message"))
                .As(typeof(ExecuteActivityFactory<,>))
                .InstancePerLifetimeScope();

            builder.RegisterGeneric(typeof(AutofacCompensateActivityFactory<,>))
                .WithParameter(new NamedParameter("name", "message"))
                .As(typeof(CompensateActivityFactory<,>))
                .InstancePerLifetimeScope();

            builder.RegisterAssemblyTypes(ThisAssembly)
                .AsClosedTypesOf(typeof(ExecuteActivity<>));

            builder.RegisterAssemblyTypes(ThisAssembly)
                .AsClosedTypesOf(typeof(CompensateActivity<>));

            builder.Register(context =>
            {
                var config = context.Resolve<IConfiguration>();
                var rabbitUrl = config["rabbitMq:Url"];
                var busControl = Bus.Factory.CreateUsingRabbitMq(cfg =>
                {
                    var host = cfg.Host(new Uri(rabbitUrl), rmq =>
                    {
                        rmq.Username("guest");
                        rmq.Password("guest");
                    });

                    var compensateUri = new Uri($"{rabbitUrl}/{ReceiveEndpointsConstants.FirstParticipantCompensate}");
                    
                    cfg.ReceiveEndpoint(host, ReceiveEndpointsConstants.FirstParticipant, ep =>
                    {
                        ep.ExecuteActivityHost(compensateUri, context.Resolve<ExecuteActivityFactory<ReverseItActivity, IReverseItArgs>>());
                    });

                    cfg.ReceiveEndpoint(host, ReceiveEndpointsConstants.FirstParticipantCompensate, ep =>
                    {
                        ep.CompensateActivityHost(context.Resolve<CompensateActivityFactory<ReverseItActivity, IReverseItLog>>());
                    });

                });
                return busControl;
            }).As<IBusControl>().SingleInstance();
        }
    }
}
