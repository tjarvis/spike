﻿using System;
using System.Linq;
using System.Threading.Tasks;
using MassTransit.Courier;
using Serilog;
using Xap.KidsXap.RoutingSlip.Common;

namespace Xap.KidsXap.RoutingSlip.FirstParticipant
{
    public class ReverseItActivity : ExecuteActivity<IReverseItArgs>, CompensateActivity<IReverseItLog>
    {
        private ILogger _logger;
        public ReverseItActivity(ILogger logger)
        {
            _logger = logger;
        }

        public async Task<ExecutionResult> Execute(ExecuteContext<IReverseItArgs> context)
        {
            try
            {
                var txt = context.Arguments.Text;
                var log = new ReverseItLog {Text = txt};

                if (txt.ToLower() == "error1")
                {
                    throw new Exception("I don't know how to reverse that");
                }

                var reversedText = new string(txt.Reverse().ToArray());
                _logger.Information($"The input reversed is {reversedText} - doing something with it now.");

                await Task.CompletedTask;
                return context.Completed(log);
            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message);
                return context.Faulted(ex);
            }
        }

        public Task<CompensationResult> Compensate(CompensateContext<IReverseItLog> context)
        {
            var log = context.Log;
            _logger.Information($"You know that thing I did with the reversed string - I just undid it - the original string was {log.Text}");
            return Task.FromResult(context.Compensated());
        }
    }



}
