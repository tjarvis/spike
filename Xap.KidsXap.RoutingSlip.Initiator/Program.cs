﻿using System;
using Autofac;
using MassTransit;
using MassTransit.Courier;
using Microsoft.Extensions.Configuration;
using Serilog;
using Xap.KidsXap.RoutingSlip.Common;

namespace Xap.KidsXap.RoutingSlip.Initiator
{
    class Program
    {
        static void Main(string[] args)
        {
            // Configure App and setup dependency injection container.
            var builder = new ContainerBuilder();
            builder.RegisterModule<AppConfigurationRegistrations>();
            builder.RegisterModule<MassTransitRegistration>();
            var container = builder.Build();
            // Tell the user we are all set and ready to go.
            var config = container.Resolve<IConfiguration>();
            var logger = container.Resolve<ILogger>();
            var hostUrl = config["rabbitMq:Url"];
            logger.Information("Logging and configuration registered");
            logger.Information($"The rabbit url is : {hostUrl}");
            // Start the bus.
            var bus = container.Resolve<IBusControl>();
            bus.Start();
            try
            {
                logger.Information($"Application and message bus started - Waiting for Input{Environment.NewLine}Type quit to exit. Type some text to test, type error1 or error2 to test the compensating paths.");
                for (;;)
                {
                    var txt = Console.ReadLine();
                    if (txt.ToLower() == "quit")
                    {
                        break;
                    }
                    if (string.IsNullOrEmpty(txt))
                    {
                        continue;
                    }

                    logger.Information($"Sending text {txt} to routing slip....");
                    StartRoutingSlip(bus, hostUrl, txt);
                }
            }
            finally
            {
                bus.Stop();
                container.Dispose();
            }
        }

        private static void StartRoutingSlip(IBusControl bus, string hostUrl, string text)
        {
           
            var builder = new RoutingSlipBuilder(Guid.NewGuid());
            var rArgs = new ReverseItArgs{Text = text};
            var mArgs = new MutateArgs{Text = text};

            builder.AddActivity("Reverse", new Uri($"{hostUrl}/{ReceiveEndpointsConstants.FirstParticipant}"), rArgs);
            builder.AddActivity("Mutate", new Uri($"{hostUrl}/{ReceiveEndpointsConstants.SecondParticipant}"), mArgs);

            var slip = builder.Build();

            bus.Execute(slip).Wait();

        }


    }
}
