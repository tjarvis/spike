﻿using System;
using Autofac;
using MassTransit;
using Microsoft.Extensions.Configuration;

namespace Xap.KidsXap.RoutingSlip.Initiator
{
    public class MassTransitRegistration : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.Register(context =>
            {
                var config = context.Resolve<IConfiguration>();
                var rabbitUrl = config["rabbitMq:Url"];
                var busControl = Bus.Factory.CreateUsingRabbitMq(cfg =>
                {
                    var host = cfg.Host(new Uri(rabbitUrl), rmq =>
                    {
                        rmq.Username("guest");
                        rmq.Password("guest");
                    });
                });
                return busControl;
            }).As<IBusControl>().SingleInstance();
        }
    }
}
