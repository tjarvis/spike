﻿namespace Xap.KidsXap.RoutingSlip.Common
{
    public class ReceiveEndpointsConstants
    {
        public const string FirstParticipant = "FirstParticipant";
        public const string FirstParticipantCompensate = "FirstParticipantCompensate";
        public const string SecondParticipant = "SecondParticipant";
        public const string SecondParticipantCompensate = "SecondParticipantCompensate";
    }
}
