﻿namespace Xap.KidsXap.RoutingSlip.Common
{
    public interface IMutateArguments
    {
        string Text { get; set; }
    }

    public interface IMutateLog
    {
        string Text { get; set; }
    }

    public class MutateArgs : IMutateArguments
    {
        public string Text { get; set; }
    }

    public class MutateLog : IMutateLog
    {
        public string Text { get; set; }
    }
}
