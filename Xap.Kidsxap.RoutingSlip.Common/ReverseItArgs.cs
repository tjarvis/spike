﻿namespace Xap.KidsXap.RoutingSlip.Common
{
    public interface IReverseItArgs
    {
        string Text { get; set; }
    }

    public interface IReverseItLog
    {
        string Text { get; set; }
    }

    public class ReverseItArgs : IReverseItArgs
    {
        public string Text { get; set; }
    }

    public class ReverseItLog : IReverseItLog
    {
        public string Text { get; set; }
    }
}
