using System;

namespace Xap.MongoRepository
{
    public interface IDocument<T>
    {
        T Id { get; set; }
    }
}