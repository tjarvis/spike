using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using MongoDB.Driver;
using MongoDB.Driver.Linq;

namespace Xap.MongoRepository
{
    
    public abstract class QueryBase<T> : IQuery<T>
    {
        private readonly IMongoDatabase _db;

        private QueryBase() { }

        protected  QueryBase(IMongoDatabase db)
        {
            _db = db;
        }

        protected IMongoQueryable<TEntity> GetQuery<TEntity>()
        {
            return _db.GetCollection<TEntity>(typeof(TEntity).Name).AsQueryable();
        }

        protected IMongoQueryable<TEntity> GetQueryWithFilter<TEntity, TKey>(Expression<Func<TEntity,bool>> filter) where TEntity : IDocument<TKey>
        {
            var query = _db.GetCollection<TEntity>(typeof(TEntity).Name).AsQueryable();
            return query.Where(filter);
        }
        
        public abstract Task<(int total, IList<T> results)> Execute<TCriteria>(TCriteria criteria, int skip = 0, int take = 50) where TCriteria : class;
    }
}