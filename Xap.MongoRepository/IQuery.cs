using System.Collections.Generic;
using System.Threading.Tasks;

namespace Xap.MongoRepository
{
    public interface IQuery<T>
    {
        Task<(int total, IList<T> results)> Execute<TCriteria>(TCriteria criteria, int skip = 0, int take = 50) where TCriteria : class;
    }
}